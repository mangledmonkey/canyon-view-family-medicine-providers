<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.mangledmonkeymedia.com
 * @since      1.7.0
 *
 * @package    Canyon_View_Family_Medicine_Providers
 * @subpackage Canyon_View_Family_Medicine_Providers/search/partials
 */
?>

<div id="providerSearch" class="main-content-container container">
  <div id="providerSearchWrapper">
    <form id="providerSearchForm">
      <div class="provider-search-container">
        <input id="providerSearchField" type="text" name="searchBar" placeholder="Search by provider name...">
      </div>
      <div id="searchFilter" style="display: none;"><a href="javascript:void(0)" class="search-filter"></i>Filter Options<i class="fa fa-caret-down" id="filterArrow" aria-hidden="true"></i></a></div>
      <div id="advancedFilter" style="display: block;"></div>
      <div id="resetFilters" style="display: none;"><a href="javascript:void(0)" class="reset-filters">reset filters</a></div>
    </form>
  </div>
  <div id="providerResults"></div>
</div>
