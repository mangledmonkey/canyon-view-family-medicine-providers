(function( $ ) {
	'use strict';

	$(document).ready(function() {
		const PROVIDERRESTURL = '/wp-json/cvfmp/v1/providers/all'
		// const FILTERSRESTURL = '/wp-json/cvfmp/v1/filters/all'
		var advancedFilter = document.getElementById('advancedFilter')
		var providerResults = document.getElementById('providerResults')
		var searchFilter = document.getElementById('searchFilter')
		var restetFilters = document.getElementById('resetFilters')
		var providers = []
		var filters = []
		let autocomplete_arr = []

		let init = function() {
			requestProvidersData( PROVIDERRESTURL )
		}

		let showLoader = function( element, id ) {
			let loader = document.createElement('div')
			loader.setAttribute('id', id)
			loader.setAttribute('class', 'search-loader')
			element.appendChild(loader)
		}

		let destroyLoader = function( id ) {
			let loader = document.getElementById(id)
			loader.remove()
		}

		let requestProvidersData = function( RESTURL ) {
			// Display loader while processing
			showLoader( advancedFilter, 'providers-loader' )

			$.get( RESTURL )
				.done( function ( response ) {
					providers = response
					populateProviders( providers )
					populateFilterData( providers, filters )
					populateFilters( filters )
					searchFilter.style.display = "block"
					advancedFilter.style.display = "none"
			    destroyLoader('providers-loader')
				}
			)
		}

		let populateProviders = function( providers ) {
			let providerCards = document.createElement('div')
			providerCards.setAttribute( 'id', 'provider-cards')

			for (var i = 0; i < providers.length; i++) {
				const provider = providers[i]
				
				const fullName = provider['first_name'] + ' ' + provider['last_name'] + ', ' + provider['title']
				const cardId = provider['first_name'] + '-' + provider['last_name'] + '-' + provider['title']
				// let card = ''
				// Create card div
				let card = document.createElement('div')
				card.setAttribute('id', cardId)
				card.setAttribute('class', 'provider-card')
				card.setAttribute('style', 'display: none;')

				// Create image div
				let photo = document.createElement('div')
				photo.setAttribute('class', 'provider-photo')

				let photo_link = document.createElement('a')
				photo_link.setAttribute('href', provider['page_url'])
				photo_link.setAttribute('target', '_blank')

				let img = document.createElement('img')
				img.setAttribute('src', provider['photo_url'])
				img.setAttribute('alt', 'Photo of ' + fullName)

				// Add photo to card
				photo_link.appendChild(img)
				photo.appendChild(photo_link)
				card.appendChild(photo)

				// Create provider-data
				let data = document.createElement('div')
				data.setAttribute('class', 'provider-data')

				// Create heading
				let name = document.createElement('div')
				name.setAttribute('class', 'provider-name')
				let heading = document.createElement('h5')
				let name_link = document.createElement('a')
				name_link.setAttribute('href', provider['page_url'])
				name_link.setAttribute('target', '_blank')
				name_link.append(fullName)

				// Add heading to provider-data
				heading.appendChild(name_link)
				name.appendChild(heading)
				data.appendChild(name)

				// Create specialties element
				let specialties = document.createElement('div')
				specialties.setAttribute('class', 'provider-specialties')
				specialties.append('Specialties: ')
				for ( let s = 0; s < provider['specialties'].length; s++) {
				// for ( const specialty of provider['specialties'] ) {
					let specialty = provider['specialties'][s]
					let specialty_link = document.createElement('a') 
					specialty_link.setAttribute('href', specialty['specialty_url'])
					specialty_link.setAttribute('target', '_blank')
					specialty_link.append(specialty['specialty'])
					specialties.appendChild(specialty_link)

					if ( s < provider['specialties'].length - 1 ) {
						specialties.append(', ')
					}
				}

				// Add specialties to provider-data
				data.appendChild(specialties)

				// Create languages element
				if ( provider['languages'] !== '' ) {
					let languages = document.createElement('div')
					languages.setAttribute('class', 'provider-languages')
					languages.append('Languages: ')
					languages.append(provider['languages'])

					// Add languages to provider-data
					data.appendChild(languages)
				}

				// Create phone number element
				let phone = document.createElement('div')
				phone.setAttribute('class', 'provider-phone')
				let phone_icon = document.createElement('i')
				phone_icon.setAttribute('class', 'fa fa-phone')
				phone_icon.setAttribute('aria-hidden', 'true')
				let phone_link = document.createElement('a')
				phone_link.setAttribute('href', 'tel:' + provider['phone_number'])
				phone_link.setAttribute('target', '_blank')
				phone_link.append(provider['phone_number'])

				// Add phone number to provider-data
				phone.appendChild(phone_icon)
				phone.appendChild(phone_link)
				data.appendChild(phone)

				// Create locations element
				let locations = document.createElement('div')
				locations.setAttribute('class', 'provider-locations')
				let location_icon = document.createElement('i')
				location_icon.setAttribute('class', 'fa fa-map-marker')
				location_icon.setAttribute('aria-hidden', 'true')
				locations.appendChild(location_icon)

				for (var l = 0; l < provider['locations'].length; l++) {
					let location = provider['locations'][l]
					let location_link = document.createElement('a')
					location_link.setAttribute('href', location['location_url'])
					location_link.setAttribute('target', '_blank')
					location_link.append(location['location'])
					locations.appendChild(location_link)

					if ( l < provider['locations'].length - 1 ) {
						locations.append(', ')
					}
				}

				// Add phone number to provider-data
				data.appendChild(locations)

				// Add card to provider cards
				card.appendChild(data)
				providerCards.appendChild(card)
			}

			providerResults.appendChild(providerCards)

		}

		//
		// Advanced Filters
		//
		$('a.search-filter').click(function() {
			const filterArrow = document.getElementById('filterArrow')
			
			if ( advancedFilter.style.display === "none" ) {
				filterArrow.className = "fa fa-caret-up"			
				advancedFilter.style.display = "block"
				resetFilters.style.display = "block"
			  // requestFiltersData( FILTERSRESTURL )
				// populateFilters()
			} else {
				filterArrow.className = "fa fa-caret-down"			
				advancedFilter.style.display = "none"
				resetFilters.style.display = "none"
				// advancedFilter.innerHTML = ''
			}
		})

		// Reset form filter options
		$('a.reset-filters').click(function() {
			const searchForm = document.getElementById('providerSearchForm')

			searchForm.reset()
			filterProviders( providers )
		})

		// Separate provider data into filters
		let populateFilterData = function( providers, filters ) {
			let genders_arr = ['Female', 'Male' ]
			let specialties_arr = [
				'Family Medicine',
				'Pediatrics',
				'Sports Medicine',
				'Women\'s Care'
			]
			let locations_set = new Set() 
			let languages_set = new Set()
			let services_set = new Set()

			// Loop through providers
			for (var i = 0; i < providers.length; i++) {
				const provider = providers[i]

				// Populate locations array
				if ( provider.locations !== null ) {
					for ( const location of provider.locations ) {
						locations_set.add( location.location )
					}
				}

				// Populate languages set
				if ( provider.languages !== '' ) {
					languages_set.add( provider.languages )
				}

				// Populate services set
				if ( provider.services !== null ) {
					for ( const service of provider.services) {
						if ( service !== '' ) {
						  services_set.add( service )
						}
					}
				}

			}

			// Create arrays from sets
			const locations_arr = Array.from( locations_set )
			const languages_arr = Array.from( languages_set )
			const services_arr  = Array.from( services_set )

			// Sort arrays
			locations_arr.sort()
			languages_arr.sort()
			services_arr.sort()

			// Add arrays to global filters array
			filters['genders'] = genders_arr
			filters['specialties'] = specialties_arr
			filters['locations'] = locations_arr
			filters['languages'] = languages_arr
			filters['services'] = services_arr

		}


		// Insert filters into page
		let populateFilters = function ( filters ) {

			// Create desktop input scaffold function
			let createInput = function ( name, id, category, type ) {
				let label = document.createElement('label')
				label.setAttribute('for', category + '-' + id)
				let input = document.createElement('input')
				input.setAttribute('type', type)
				input.setAttribute('name', category)
				input.setAttribute('value', name)
				input.setAttribute('id', category + '-' + id)
				input.setAttribute('class', 'filter-input')
				
				label.appendChild(input)
				label.append(name)

				return label
			}

			// Create mobile dropdown scaffold function
			let createSelectOption = function ( name) {
				let option   = document.createElement('option')
				// option.text  = name
				option.value = name
				option.label = name
				option.text  = name

				return option
			}

			// Create Row 1
			let filters_row_1 = document.createElement('div')
			filters_row_1.setAttribute('class', 'filters-row-1')

			// Populate genders
			let genders = document.createElement('div')
			genders.setAttribute('class', 'filter-gender filter')

			let genders_header = document.createElement('h6')
			genders_header.append('Provider\'s Gender')
			genders.appendChild(genders_header)

			let genders_desktop = document.createElement('div')
			genders_desktop.setAttribute('id', 'gendersDesktop')
			genders_desktop.setAttribute('class', 'filter-desktop')

			let genders_mobile = document.createElement('div')
			genders_mobile.setAttribute('id', 'gendersMobile')
			genders_mobile.setAttribute('class', 'filter-mobile')

			let genders_select = document.createElement('select')
			genders_select.setAttribute('id', 'gendersSelect')
			genders_select.setAttribute('name', 'gender')
			let genders_blank = document.createElement('option')
			genders_select.add(genders_blank)

			for ( const g in filters['genders'] ) {
				const gender = filters['genders'][g] 
				const gender_id = 'gender-' + gender.replace(/\s+/g, '-').toLowerCase()

				// Create the input
				const gender_desktop_input = createInput( gender, gender_id, 'gender', 'checkbox')

								// Create the mobile input
				const gender_mobile_option = createSelectOption( gender, gender_id )

				// Add desktop inputs
				genders_desktop.appendChild(gender_desktop_input)
				genders_select.add(gender_mobile_option)
			}

			// Add select to mobile div
			genders_mobile.appendChild(genders_select)

			// Add desktop inputs to specialties
			genders.appendChild(genders_desktop)
			genders.appendChild(genders_mobile)



			// Populate specialties
			let specialties = document.createElement('div')
			specialties.setAttribute('class', 'filter-specialties filter')

			let specialties_header = document.createElement('h6')
			specialties_header.append('Specialties')
			specialties.appendChild(specialties_header)

			let specialties_desktop = document.createElement('div')
			specialties_desktop.setAttribute('id', 'specialtiesDesktop')
			specialties_desktop.setAttribute('class', 'filter-desktop')

			let specialties_mobile = document.createElement('div')
			specialties_mobile.setAttribute('id', 'specialtiesMobile')
			specialties_mobile.setAttribute('class', 'filter-mobile')

			let specialties_select = document.createElement('select')
			specialties_select.setAttribute('id', 'specialtiesSelect')
			specialties_select.setAttribute('name', 'specialty')
			let specialties_blank = document.createElement('option')
			specialties_select.add(specialties_blank)

			// Create desktop inputs
			for ( const s in filters['specialties'] ) {
				const specialty = filters['specialties'][s]
				const specialty_id = 	'specialty-' + specialty.replace(/\s+/g, '-').toLowerCase()

				// Create the desktop input
				const specialty_desktop_input = createInput( specialty, specialty_id, 'specialty', 'checkbox')

				// Create the mobile input
				const specialty_mobile_option = createSelectOption( specialty, specialty_id )

				// Add desktop inputs
				specialties_desktop.appendChild(specialty_desktop_input)
				specialties_select.add(specialty_mobile_option)
			}

			// Add select to mobile div
			specialties_mobile.appendChild(specialties_select)

			// Add desktop inputs to specialties
			specialties.appendChild(specialties_desktop)
			specialties.appendChild(specialties_mobile)



			// // Populate locations
			let locations = document.createElement('div')
			locations.setAttribute('class', 'filter-locations filter')

			let locations_header = document.createElement('h6')
			locations_header.append('Locations')
			locations.appendChild(locations_header)

			let locations_desktop = document.createElement('div')
			locations_desktop.setAttribute('id', 'locationsDesktop')
			locations_desktop.setAttribute('class', 'filter-desktop')

			let locations_mobile = document.createElement('div')
			locations_mobile.setAttribute('id', 'locationsMobile')
			locations_mobile.setAttribute('class', 'filter-mobile')

			let locations_select = document.createElement('select')
			locations_select.setAttribute('id', 'locationsSelect')
			locations_select.setAttribute('name', 'location')
			let location_blank = document.createElement('option')
			locations_select.add(location_blank)

			for( const l in filters['locations']) {
				const location = filters['locations'][l]
				const location_id = 'location-' + location.replace(/\s+/g, '-').toLowerCase()

    		// Create the input
				const location_desktop_input = createInput( location, location_id, 'location', 'checkbox')

				// Create the mobile input
				const location_mobile_option = createSelectOption( location, location_id )

				// Add desktop inputs
				locations_desktop.appendChild(location_desktop_input)
				locations_select.add(location_mobile_option)
			}

			// Add select to mobile div
			locations_mobile.appendChild(locations_select)

			// Add desktop inputs to specialties
			locations.appendChild(locations_desktop)
			locations.appendChild(locations_mobile)

			// Populate languages
			let languages = document.createElement('div')
			languages.setAttribute('class', 'filter-languages filter')

			let languages_header = document.createElement('h6')
			languages_header.append('Languages')
			languages.appendChild(languages_header)

			let languages_desktop = document.createElement('div')
			languages_desktop.setAttribute('id', 'languagesDesktop')
			languages_desktop.setAttribute('class', 'filter-desktop')

			let languages_mobile = document.createElement('div')
			languages_mobile.setAttribute('id', 'languagesMobile')
			languages_mobile.setAttribute('class', 'filter-mobile')

			let languages_select = document.createElement('select')
			languages_select.setAttribute('id', 'languagesSelect')
			languages_select.setAttribute('name', 'language')
			let language_blank = document.createElement('option')
			languages_select.add(language_blank)

			for (const k in filters['languages'] ) {
			 	const language = filters['languages'][k]
 			 	const language_id = 'language-' + language.replace(/\s+/g, '-').toLowerCase()
				
				// Create the input
				const language_desktop_input = createInput( language, language_id, 'language', 'checkbox')

				// Create the mobile input
				const language_mobile_option = createSelectOption( language, language_id )

				// Add desktop inputs
				languages_desktop.appendChild(language_desktop_input)
				languages_select.add(language_mobile_option)
			}

			// Add select to mobile div
			languages_mobile.appendChild(languages_select)

			// Add desktop inputs to specialties
			languages.appendChild(languages_desktop)
			languages.appendChild(languages_mobile)

			// Create Row 2
			// let filters_row_2 = document.createElement('div')
			// filters_row_2.setAttribute('class', 'filters-row-2')

			// Populate services
			let services = document.createElement('div')
			services.setAttribute('class', 'filter-services filter')

			let services_header = document.createElement('h6')
			services_header.append('Services')
			services.appendChild(services_header)

			// let services_desktop = document.createElement('div')
			// services_desktop.setAttribute('id', 'servicesDesktop')
			// services_desktop.setAttribute('class', 'filter-desktop')

			// let services_mobile = document.createElement('div')
			// services_mobile.setAttribute('id', 'servicesMobile')
			// services_mobile.setAttribute('class', 'filter-mobile')

			let services_select = document.createElement('select')
			services_select.setAttribute('id', 'servicesSelect')
			services_select.setAttribute('name', 'service')
			services_select.multiple = true

			// Add default empty selection
			let services_blank = document.createElement('option')
			services_blank.setAttribute('label', '- clear selection -')
			services_blank.setAttribute('text', '- clear selection -')
			services_blank.setAttribute('value', '')
			services_select.add(services_blank)

			// Add 'All Services' option
			let all_services_option = createSelectOption( 'All Services')
			services_select.add(all_services_option)

			// Create services columns
			// let services_col_1 = document.createElement('div')
			// services_col_1.setAttribute('class', 'services-col-1')

			// let services_col_2 = document.createElement('div')
			// services_col_2.setAttribute('class', 'services-col-2')

			// let services_col_3 = document.createElement('div')
			// services_col_3.setAttribute('class', 'services-col-3')

			// Set services loop variables
			// let s_count = 0
			// let s_length = Object.keys(filters['services']).length / 3

			// Generate services inputs
			for (const s in filters['services']) {
			 	const service = filters['services'][s]
 			 	// const service_id = 'service-' + service.replace(/\s+/g, '-').toLowerCase()
				
				// Create the input
				// const service_desktop_input = createInput( service, service_id, 'service', 'checkbox')

				// Create the mobile input
				const service_mobile_option = createSelectOption( service )

				// Add desktop input to services column
				// if ( s_count >= s_length  && s_count <  ( s_length * 2 ) ) {
				// 	services_col_2.appendChild(service_desktop_input)
				// } else if ( s_count >= ( s_length * 2 ) ) {
				// 	services_col_3.appendChild(service_desktop_input)
				// } else {
				// 	services_col_1.appendChild(service_desktop_input)
				// }

				// Add desktop inputs
				services_select.add(service_mobile_option)

				// s_count++
			}
			// Add desktop inputs to services
			// services_desktop.appendChild(services_col_1)
			// services_desktop.appendChild(services_col_2)
			// services_desktop.appendChild(services_col_3)

			// Add select to mobile div
			// services_mobile.appendChild(services_select)

			// Add inputs to specialties
			// services.appendChild(services_desktop)
			// services.appendChild(services_mobile)
			services.appendChild(services_select)


			// Add message after select box
			let services_message = document.createElement('div')
			services_message.setAttribute('class', 'services-message')
			services_message.innerText = "(CTRL/CMD + CLICK to select multiple)"

			services.appendChild(services_message)

			// Add locations to filter elements
			filters_row_1.appendChild(locations)
			// Add specialties to filter elements
			filters_row_1.appendChild(specialties)
			// Add genders to filter elements
			filters_row_1.appendChild(genders)
			// Add languages to filter elements
			filters_row_1.appendChild(languages)
			// Add services to filter elements
			filters_row_1.appendChild(services)


			// Add filters to page
			advancedFilter.appendChild(filters_row_1)
			// advancedFilter.appendChild(filters_row_2)
		}

		
		// Provider filtering
		let filterProviders = function( providers ) {
			const searchField = document.getElementById('providerSearchField')
			const input_filters = document.getElementsByTagName('input')
			const select_filters = document.getElementsByTagName('select')
			let autocomplete_set = new Set()
			let filters = []
			let active_filters = []

			for ( let filter of input_filters ) {
				filters.push(filter)
			}

			for ( let filter of select_filters ) {
				filters.push(filter)
			}

			// Add search text to filters array if not empty
			if (searchField.value !== '') {
				active_filters.push(searchField)
			}

			// Combine input and select filters into array
			for ( let i = 0; i < filters.length; i++ ) {
				let filter = filters[i]
			
				if ( filter.nodeName === 'INPUT' ) {
					if ( filter.checked  ) {

						// Add value to filters array
						active_filters.push( filter )
					}
				} else if ( filter.nodeName === 'SELECT') {
					if ( filter.value !== '' ) {

						// Add value to filters array
						active_filters.push( filter )
					}
				}

			}

			// console.log(active_filters.length)

			// console.log(active_filters)

			let regexCheck = function( filter, value ) {
				let pattern = new RegExp( filter, "gi")
				if ( value !== 0 ) {
					if ( pattern.test( value ) ) {
						return true
					}
					return false
				}
			}

			let filterCheck = function( filter, array, key ) {
				let present = false
				let present_arr = []

				if ( array !== null ) {
					for ( let item of array ) {
						if ( key ) {
							item = item[key]
						}

						if ( regexCheck( filter.value, item ) ) {
							present = true
						}
					}
				}

				present_arr[filter.value] = present 

				return present_arr 
			}

			for (var i = 0; i < providers.length; i++) {
				const provider = providers[i]
				const provider_name = provider['first_name'] + ' ' + provider['last_name'] + ', ' + provider['title']
				const cardId = provider['first_name'] + '-' + provider['last_name'] + '-' + provider['title']
				const card = document.getElementById(cardId)
				let search_present    = { passed: false, present: [] }
				let gender_present    = { passed: false, present: [] }
				let specialty_present = { passed: false, present: [] }
				let location_present  = { passed: false, present: [] }
				let language_present  = { passed: false, present: [] }
				var service_present   = { passed: false, present: [] }
				let filter_present    = false

				// Find filters in provider object
				for ( const filter of active_filters ) {

					if ( filter.name === 'searchBar') {
						if ( filter.value !== '' ) {
							search_present.passed = true
							let present = false
							let present_arr = []

							// Check provider object
							if (Object.values(provider).some(function(value){
								return regexCheck(filter.value, value)
							})) {
								present = true
							}

							// Check full provider name
							if ( regexCheck(filter.value, provider_name) ) {
								present = true
							}

							present_arr[filter.value] =  present
							search_present.present.push( present_arr )
						}
					}

					// Check genders
					if ( filter.name === 'gender') {
						const gender_pattern = '^' + filter.value 
						const pattern = new RegExp( gender_pattern, "gi")
						
						if ( provider.gender !== 0 ) {
							gender_present.passed = true
							let present = false
							let present_arr = []
								
							if ( pattern.test( provider.gender ) ) {
								present = true
							}

							present_arr[filter.value] =  present
							gender_present.present.push( present_arr )
						}
					} 

					// Check specialties
					if ( filter.name === 'specialty') {
						if ( provider.specialties !== null ) {
							specialty_present.passed = true

							let present_arr = filterCheck( filter, provider.specialties, 'specialty')
							// console.log(present_arr)
							// let key = Object.keys(present_arr)[0]

							// if ( present_arr[key] === true ) {
								specialty_present.present.push( present_arr ) 
							// }
						}
					}

					// Check locations
					if ( filter.name === 'location') {
						if ( provider.locations !== null ) {
							location_present.passed = true
							let present_arr = filterCheck( filter, provider.locations, 'location')
							location_present.present.push(present_arr)
						}
					}

					// Check languages
					if ( filter.name === 'language') {
						if ( provider.languages !== null ) {
						  language_present.passed = true
							let present_arr = []
							let present = regexCheck(filter.value, provider.languages)
							present_arr[filter.value] =  present
							language_present.present.push(present_arr)
						}
					}

					// Check services
					if ( filter.name === 'service' ) {
						console.log (filter.options)
						if ( provider.services !== null ) {
							service_present.passed = true

							Array.from( filter.options ).forEach(function(option_element){
								if (option_element.selected === true) {
									let present_arr = []
									console.log(option_element)

									// Handle 'All Services' selection
									if ( option_element.value === "All Services" ) {
										present_arr['All Services'] = true
									} else {
										present_arr = filterCheck( option_element, provider.services)
									}

									console.log(present_arr)
									service_present.present.push( present_arr )

								}

							})
						}
					}

				}

				console.log( service_present )

				// Check if passed filter is also present function
				let checkPresent = function ( item ) {
					let status = false

					if ( item.passed && item.present.length !== 0 ) {
						for ( let p of item.present ) {
							let key = Object.keys(p)[0]

							if ( p[key] === true ) {
								status = true

								console.log(key + ': ' + status )
							}
						}
						
					} else if ( !item.passed ) {
						status = true
					}

					return status
				}

				// Check if all filters are present for the provider
				if ( checkPresent(search_present)
					&& checkPresent(gender_present)
					&& checkPresent(specialty_present)
					&& checkPresent(location_present)
					&& checkPresent(language_present)
					&& checkPresent(service_present) 
					) {
					filter_present = true

					// Add to autocomplete
					autocomplete_set.add(provider_name)
				}




				// Show or hide provider card
				if ( filter_present === true ) {
					// console.log( 'Present: ' + cardId )
					card.style.display = "block"
				} else {
					card.style.display = "none"
				}

				// Reset if no search values
				if ( searchField.value === '' && active_filters.length === 0 ) {
					card.style.display = "none"
				}

			}

			// Push autocomplete set to sorted array
			autocomplete_arr = Array.from(autocomplete_set).sort()

			// Autocomplete
			$( 'input[name="searchBar"]' ).autocomplete({
				source: autocomplete_arr,
				select: function( event, ui ) {
					$(this).val(ui.item.value)
					filterProviders( providers )
				},
				open: function(){
					let width = $('.ui-autocomplete-input').width() + 30
						$('.ui-autocomplete').css('width', width)
				}
			})

		}



		// Detect search text field changes
		$('#providerSearchField').on('input', function() {
			filterProviders( providers )
		})

		// Disable form submit on Enter
		$('#providerSearchField').keypress(
			function(event){
				if (event.which == '13') {
					event.preventDefault();
				}
		});



		// Detect checkbox changes
		advancedFilter.addEventListener('change', function(event) {
			const targetElement = event.target;
			const selector = 'input'

			if (targetElement.matches(selector)){
				// const filters = document.getElementsByTagName('input')
				filterProviders( providers )
			}
		})

		// Detect select option changes
		advancedFilter.addEventListener('change', function(event) {
			const targetElement = event.target;
			const selector = 'select'

			if (targetElement.matches(selector)){
				// const filters = document.getElementsByTagName('select')
				filterProviders( providers )
			}
		})


		init()
	})


})( jQuery );
