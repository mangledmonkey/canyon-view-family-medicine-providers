<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.mangledmonkeymedia.com
 * @since      1.7.00
 *
 * @package    Canyon_View_Family_Medicine_Providers
 * @subpackage Canyon_View_Family_Medicine_Providers/search
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Canyon_View_Family_Medicine_Providers
 * @subpackage Canyon_View_Family_Medicine_Providers/search
 * @author     Brandon West <brandon@mangledmonkeymedia.com>
 */
class Canyon_View_Family_Medicine_Providers_Search {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.7.00
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.7.00
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.7.00
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name . '-search';
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.7.00
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Family_Medicine_Providers_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Family_Medicine_Providers_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/canyon-view-family-medicine-providers-search.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.7.00
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Family_Medicine_Providers_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Family_Medicine_Providers_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
    if ( is_page ( array('find-a-provider'))) {
		  wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/canyon-view-family-medicine-providers-search.js', array( 'jquery' ), $this->version, false );

      // JQuery UI Autocomplete
      wp_enqueue_script( 'jquery-ui-core' );
      wp_enqueue_script( 'jquery-ui-autocomplete' );
    }

	}

	/**
	 * Register the shortcodes for the public-facing side of the site.
	 *
	 * @since    1.7.00
	 */
	public function register_shortcodes()
  {

		add_shortcode( 'cvfmp_provider_search', array( $this, 'provider_search_shortcode' ) );

 	}


  /**
	 * Register REST route.
	 *
	 * @since    1.7.00
	 */
	public function register_providers_search_api()
  {
    register_rest_route('cvfmp/v1', '/providers', array(
      'methods' => 'GET',
      'callback' => array($this, 'search_api_get_providers_callback'),
    ) );

    register_rest_route('cvfmp/v1', '/providers/all', array(
      'methods' => 'GET',
      'callback' => array($this, 'search_api_get_all_providers_callback'),
    ) );

    register_rest_route('cvfmp/v1', '/services', array(
      'methods' => 'GET',
      'callback' => array($this, 'search_api_get_services_callback'),
    ) );

    register_rest_route('cvfmp/v1', '/services/all', array(
      'methods' => 'GET',
      'callback' => array($this, 'search_api_get_all_services_callback'),
    ) );

    register_rest_route('cvfmp/v1', '/locations', array(
      'methods' => 'GET',
      'callback' => array($this, 'search_api_get_locations_callback'),
    ) );

    register_rest_route('cvfmp/v1', '/locations/all', array(
      'methods' => 'GET',
      'callback' => array($this, 'search_api_get_all_locations_callback'),
    ) );

    register_rest_route('cvfmp/v1', '/languages', array(
      'methods' => 'GET',
      'callback' => array($this, 'search_api_get_languages_callback'),
    ) );

    register_rest_route('cvfmp/v1', '/languages/all', array(
      'methods' => 'GET',
      'callback' => array($this, 'search_api_get_all_languages_callback'),
    ) );

    register_rest_route('cvfmp/v1', '/filters', array(
      'methods' => 'GET',
      'callback' => array($this, 'search_api_get_filters_callback'),
    ) );

    register_rest_route('cvfmp/v1', '/filters/all', array(
      'methods' => 'GET',
      'callback' => array($this, 'search_api_get_all_filters_callback'),
    ) );

 	}



  /**
  * shortcode [cvfmp_provider_search]
  *
  * @since    1.7.0
  */
  public function provider_search_shortcode( $atts )
  {

    require( plugin_dir_path( __FILE__ ) . 'partials/canyon-view-family-medicine-providers-search-display.php' );

  }


  /**
  * Get all pages from parent name ( Providers | Locations | Services )
  * 
  * @since    1.7.4
  */
  function get_pages_from_parent( $parent_name )
  {
    // Get parent page ID
    $parent = get_page_by_title( $parent_name );

    // Specify provider pages search criteria
    $page_args = array(
      'child_of' 		=> $parent->ID,
      'sort_order'     => 'ASC',
      'sort_column'  => 'post_title',
      'post_status' => 'publish'
    );

    // Get provider pages IDs
    $pages = get_pages( $page_args );

    return $pages;
  }


  /**
  * Search routing to satellite sites
  * ( $providers || $services )
  * 
  * @since    1.7.4
  */
  function search_satellites( $category )
  {
    $site_url = get_site_url(); 
    $domains = array('canyonviewfamilymedicine.com', 'canyonviewsportsmedicine.com', 'canyonviewpediatrics.com', 'canyonviewwomenscare.com');
    $search_data = [];
   
    // request data from all domains
    foreach ( $domains as $domain ) {
      // Run internal function if calling from current website
      if ( strpos( $site_url, $domain )) {
        switch ($category) {
          case "providers":
            $request_data = $this->search_api_get_providers_callback();
            break;
          case "services":
            $request_data = $this->search_api_get_services_callback();
            break;
          case "locations":
            $request_data = $this->search_api_get_locations_callback();
            break;
          case "languages":
            $request_data = $this->search_api_get_languages_callback();
            break;
          case "filters":
            $request_data = $this->search_api_get_filters_callback();
            break;
        }
      } else {
        // Call external API for remove websites
        $remote = 'https://' . $domain . '/wp-json/cvfmp/v1/' . $category;
        
        if ( $remote !== '' ) {
          $request = wp_remote_get( $remote );
    
          if( is_wp_error( $request ) ) {
            return false; // Bail early
          }
          
          $body = wp_remote_retrieve_body( $request );
    
          $request_data = json_decode( $body );
        }
      }
     
      // Add to collected array
      foreach( $request_data as $data ) {
        array_push( $search_data, $data);
      }

    }

    return $search_data;
  }

   /**
  * Search routing to satellite sites asynchronously
  * ( $providers || $services )
  * 
  * @since    1.7.09
  */
  function search_satellites_async( $category )
  {
    $site_url = get_site_url(); 
    $domains = [
      'fm'    => 'canyonviewfamilymedicine.com',
      'spmed' => 'canyonviewsportsmedicine.com',
      'peds'  => 'canyonviewpediatrics.com',
      'wc'    => 'canyonviewwomenscare.com'
    ];
    $request_data = [];
    $search_data = [];

    // Define requests for each domain
    foreach ( $domains as $domain ) {
      $url = 'https://' . $domain . '/wp-json/cvfmp/v1/' . $category;
      $request = [
        'url' => $url,
        'type' => 'GET'
      ];
      array_push( $request_data, $request );

    }

    // Make requests
    $responses = Requests::request_multiple( $request_data );

    // Extract required information
    foreach ( $responses as $response ){
      $data = [];
      if( is_a( $response, 'Requests_Response' ) ){
        $data = json_decode( $response->body );

        foreach ( $data as $item ) {
          array_push( $search_data, $item );
        }
      }
    }

    return $search_data;
  }


  /**
	 * Returns a providers data object
   * 
   * @since    1.7.1
	 */
  function search_api_get_providers_callback() {
    // Get website specialty
    $specialty = 'localhost';
    $site_url = get_site_url();
    if ( strpos( $site_url, 'familymedicine' )) {
      $specialty = "Family Medicine";
    } elseif ( strpos( $site_url, 'sportsmedicine' )) {
      $specialty = "Sports Medicine";
    } elseif ( strpos( $site_url, 'pediatrics' )) {
      $specialty = "Pediatrics"; 
    } elseif ( strpos( $site_url, 'womenscare' )) {
      $specialty = "Women's Care";
    }

    // Initialize the array that will receive the providers data.
    $providers_data = array();

    // Get provider pages
    $providers = $this->get_pages_from_parent('Providers');

    // Collect relevant provider pages data
    // name
    // gender
    // specialties
    // locations
    // services
    // languages
    foreach ($providers as $provider) {
      $locations_array = [];
      $location_links_array = [];
      $services_array = [];
      $specialties_array = [];

      $metadata = get_post_meta($provider->ID);

      // Provider page URL
      $page_url = get_page_link($provider->ID);

      // Add domain to photo URL
      $photo_url = $metadata['_cvfmp_meta_provider_image_url'][0];
      if ( strpos( $photo_url, $site_url ) === false ) {
        $photo_url = $site_url . $photo_url;
      }
      
      // Clean up locations array
      $locations = $metadata['_cvfmp_meta_provider_locations'][0];
      if (preg_match_all('/"([^"]+)"/', $locations, $m, 0 )) {
          array_push( $locations_array, $m[1] );
      }

      // Get links to location pages
      foreach ( $locations_array[0] as $location ) {
        $location_url = get_permalink( get_page_by_title( $location ) );
        $location_array = array( "location" => $location, "location_url" => $location_url );
        array_push( $location_links_array, $location_array );
      }

      // Clean up services array
      $services = $metadata['_cvfmp_meta_services'][0];
      if (preg_match_all('/"([^"]+)"/', $services, $m, 0 )) {
        array_push( $services_array, $m[1] );
      }

      // Clean up languages string
      $languages = $metadata['_cvfmp_meta_languages'][0];
      $languages_array = explode("; ", $languages);

      $specialty_data = array([ 'specialty' => $specialty, 'specialty_url' => $site_url ]);
      array_push( $specialties_array, $specialty_data );
 
      $provider_array = array(
        'first_name'   => $metadata['_cvfmp_meta_provider_name_first'][0],
        'last_name'    => $metadata['_cvfmp_meta_provider_name_last'][0],
        'title'        => $metadata['_cvfmp_meta_provider_name_title'][0],
        'photo_url'    => $photo_url,
        'page_url'     => $page_url,
        'gender'       => $metadata['_cvfmp_meta_provider_gender'][0],
        'phone_number' => $metadata['_cvfmp_meta_phone_number'][0],
        'locations'     => $location_links_array,
        'services'     => $services_array[0],
        'languages'    => $languages_array[0],
        'specialties'    => $specialty_data
      );

      array_push( $providers_data, $provider_array );

    }
    
    // echo "<pre>";
    // print_r( $providers );
    // echo "</pre>";

    return $providers_data;
      
  }

  /**
  * Returns a data object of all providers via custom API
  * 
  * @since    1.7.2
  */
  function search_api_get_all_providers_callback()
  {
    $cleaned_providers = [];
    $providers = $this->search_satellites_async("providers");

    // Merge duplicate providers
    foreach( $providers as $provider ) {
      $unique = true;

      if ( !empty($cleaned_providers)) {
      // if ( $provider->last_name == 'Cameron' ) {
        $first_name  = $provider->first_name;
        $last_name   = $provider->last_name;
        $title       = $provider->title;
        $locations   = $provider->locations;
        $services    = $provider->services;
        $specialties = $provider->specialties;

        foreach ( $cleaned_providers as $key=>&$cleaned_provider ) {
          if ( $last_name == $cleaned_provider->last_name && $first_name == $cleaned_provider->first_name && $title == $cleaned_provider->title ) {
            $unique = false;
            $merged_locations = [];
            $merged_services  = [];
            $merged_specialties = [];
            
            // Merge services 
            foreach ( $services as $service ) {
              array_push( $merged_services, $service );
            }
            foreach ( $cleaned_provider->services as $service) {
              array_push( $merged_services, $service );
            }

            // Merge specialties
            foreach ( $specialties as $specialty ) {
              array_push( $merged_specialties, $specialty );
            }
            foreach ( $cleaned_provider->specialties as $specialty ) {
              array_push( $merged_specialties, $specialty );
            }

            // Merge locations
            foreach ( $locations as $location ) {
              array_push( $merged_locations, $location );
            }
            foreach ( $cleaned_provider->locations as $location ) {
              array_push( $merged_locations, $location );
            } 

            // Remove duplicates
            $merged_services  = array_unique( $merged_services );

            // Sort data
            natcasesort( $merged_services );

            // Fix sorting indexes
            $merged_services = array_values($merged_services);
            

            // Update existing record
            $cleaned_provider->services = $merged_services;
            $cleaned_provider->specialties = $merged_specialties;
            $cleaned_provider->locations = $merged_locations;
          }
        } 
        
        if ( $unique == true ) {
          array_push( $cleaned_providers, $provider ); 
        }

      } else {
        array_push( $cleaned_providers, $provider );
      }
      
       
    }

    // Multidimensional sort
    array_multisort( 
      array_column( $cleaned_providers, 'last_name' ), SORT_ASC, 
      array_column( $cleaned_providers, 'first_name' ), SORT_ASC,
      $cleaned_providers
    );



    // echo "<pre>";
    // print_r( $all_providers_data );
    // echo "</pre>";

    return $cleaned_providers;

  }


  /**
	 * Returns a services data object
   * 
   * @since    1.7.4
	 */
  function search_api_get_services_callback() {

    // Initialize the array that will receive the services data
    $services_data = array();

    // Get provider pages IDs
    $services = $this->get_pages_from_parent('Services');

    // Extract service names
    foreach ( $services as $service ) {
      array_push( $services_data, $service->post_title );
    }

    return $services_data;

  }


  /**
	 * Returns a data object of all services via custom API
   * 
   * @since    1.7.4
	 */
  function search_api_get_all_services_callback()
  {
    $services = $this->search_satellites_async("services");

    return $services;

  }


  /**
	 * Returns a locations data object
   * 
   * @since    1.7.4
	 */
  function search_api_get_locations_callback() {

    // Initialize the array that will receive the services data
    $locations_data = array();

    // Get provider pages IDs
    $locations = $this->get_pages_from_parent('Locations');

    // Extract service names
    foreach ( $locations as $location ) {
      array_push( $locations_data, $location->post_title );
    }

    return $locations_data;
  }


  /**
	 * Returns a data object of all locations via custom API
   * 
   * @since    1.7.4
	 */
  function search_api_get_all_locations_callback()
  {
    $locations = $this->search_satellites_async("locations");

    // Remove duplicates
    $locations = array_unique( $locations );

    return $locations;

  }


  /**
	 * Returns a languages data object
   * 
   * @since    1.7.4
	 */
  function search_api_get_languages_callback()
  {

    // Initialize the array that will receive the services data
    $languages_data = array();

    // // Get provider pages IDs
    $providers = $this->get_pages_from_parent('Providers');

    // Get languages from provider pages
    foreach ( $providers as $provider ) {
      $languages_meta = get_post_meta($provider->ID, '_cvfmp_meta_languages', true);
      $languages = explode(";", $languages_meta);

      foreach ( $languages as $language ) {
        $language = str_replace(' ', '', $language);
        array_push( $languages_data, $language );
      }
    }

    // Remove duplicates
    $languages_data = array_unique( $languages_data );

    return $languages_data;

  }


  /**
	 * Returns a data object of all languages via custom API
   * 
   * @since    1.7.4
	 */
  function search_api_get_all_languages_callback()
  { 
    // Get provider pages
    $languages = $this->search_satellites_async("Languages");

    // Replace empty languages with English
    $languages = array_map(function($value) {
      return $value === "" ? "English" : $value;
    }, $languages);

    // Remove duplicates
    $languages = array_unique( $languages );

    return $languages;

  }


    /**
	 * Returns a filters data object
   * 
   * @since    1.7.4
	 */
  function search_api_get_filters_callback()
  {
    $locations = $this->search_api_get_locations_callback();
    $services  = $this->search_api_get_services_callback();
    $languages = $this->search_api_get_languages_callback();

    // Replace empty languages with English
    $languages = array_map(function($value) {
      return $value === "" ? "English" : $value;
    }, $languages);

    $filters_data = array(
      'locations' => $locations,
      'services'  => $services,
      'languages' => $languages
    );

    return $filters_data;

  }


  /**
	 * Returns a data object of all search filters via custom API
   * 
   * @since    1.7.4
	 */
  function search_api_get_all_filters_callback()
  {
    $services  = $this->search_satellites_async("services");
    $locations = $this->search_satellites_async("locations");
    $languages = $this->search_satellites_async("languages");

    // Remove duplicates
    $services  = array_unique( $services );
    $locations = array_unique( $locations );
    $languages = array_unique( $languages );

    // Replace empty languages with English
    $languages = array_map(function($value) {
      return $value === "" ? "English" : $value;
    }, $languages);

    // Sort data
    natcasesort( $services );
    natcasesort( $languages );
    natcasesort( $locations );

    // Fix sorting indexes
    $services = array_values($services);
    $languages = array_values($languages);
    $locations = array_values($locations);

    $filters_data = array(
      'locations' => $locations,
      'services'  => $services,
      'languages' => $languages
    );

    return $filters_data;

  }

}
