<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.mangledmonkeymedia.com
 * @since             1.0.0
 * @package           Canyon_View_Family_Medicine_Providers
 *
 * @wordpress-plugin
 * Plugin Name:       Canyon View Family Medicine Providers
 * Plugin URI:        http://www.bitbucket.com/mangledmonkey/canyon-view-family-medicine-providers
 * Description:       Adds Provider details tools to provider pages and helps automate the display of providers on the live website pages.
 * Version:           1.8.11
 * Author:            Brandon West
 * Author URI:        http://www.mangledmonkeymedia.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       canyon-view-family-medicine-providers
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-canyon-view-family-medicine-providers-activator.php
 */
function activate_canyon_view_family_medicine_providers() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-canyon-view-family-medicine-providers-activator.php';
	Canyon_View_Family_Medicine_Providers_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-canyon-view-family-medicine-providers-deactivator.php
 */
function deactivate_canyon_view_family_medicine_providers() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-canyon-view-family-medicine-providers-deactivator.php';
	Canyon_View_Family_Medicine_Providers_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_canyon_view_family_medicine_providers' );
register_deactivation_hook( __FILE__, 'deactivate_canyon_view_family_medicine_providers' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-canyon-view-family-medicine-providers.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_canyon_view_family_medicine_providers() {

	$plugin = new Canyon_View_Family_Medicine_Providers();
	$plugin->run();

}
run_canyon_view_family_medicine_providers();
