jQuery(document).ready(function($){

  var mediaUploader;

  $('#cvfmp_meta_provider_image_upload_button').click(function(e) {
    e.preventDefault();
    openMediaUploader('#cvfmp-meta-provider-image-img', '#cvfmp-meta-provider-image-url');
   });

  $('#cvfmp_meta_candid_photo_left_upload_button').click(function(e) {
    e.preventDefault();
    openMediaUploader('#cvfmp-meta-candid-photo-left-img', '#cvfmp-meta-candid-photo-left-url');
  });

  $('#cvfmp_meta_candid_photo_right_upload_button').click(function(e) {
    e.preventDefault();
    openMediaUploader('#cvfmp-meta-candid-photo-right-img', '#cvfmp-meta-candid-photo-right-url');
  });

  $('#cvfmp_meta_service_image_upload_button').click(function(e) {
    e.preventDefault();
    openMediaUploader('#cvfmp-meta-service-image-img', '#cvfmp-meta-service-image-url');
  });

  function openMediaUploader(img, input){
    // If the uploader object has already been created, reopen the dialog
    // if (mediaUploader) {
    //   mediaUploader.open();
    //   return;
    // }
    // Extend the wp.media object
    mediaUploader = wp.media.frames.file_frame = wp.media({
      title: 'Choose Image',
      button: {
        text: 'Choose Image'
      },
      multiple: false
    });

    var set_media_image = function() {
      var selection = mediaUploader.state().get('selection');

      // no selection
      if (!selection) {
        return;
      }

      // iterate through selected elements
      selection.each(function(attachment) {
        var url = attachment.attributes.url;
        $(input).val(url);
        $(img).attr('src', url);
      });
    };


    mediaUploader.on('select', set_media_image);
    mediaUploader.on('close', set_media_image);

    // Open the uploader dialog
    mediaUploader.open();
  }
  
  $('.custom_date').datepicker({
    dateFormat : 'MM d, yy'
  });
  
});
