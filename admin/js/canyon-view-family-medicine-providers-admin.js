(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	   var gk_media_init = function(selector, button_selector)  {
	     var clicked_button = false;

	     jQuery(selector).each(function (i, input) {
	         var button = jQuery(input).next(button_selector);
	         button.click(function (event) {
	             event.preventDefault();
	             var selected_img;
	             clicked_button = jQuery(this);

	             // check for media manager instance
	             if(wp.media.frames.gk_frame) {
	                 wp.media.frames.gk_frame.open();
	                 return;
	             }
	             // configuration of the media manager new instance
	             wp.media.frames.gk_frame = wp.media({
	                 title: 'Select image',
	                 multiple: false,
	                 library: {
	                     type: 'image'
	                 },
	                 button: {
	                     text: 'Use selected image'
	                 }
	             });

	             // Function used for the image selection and media manager closing
	             var gk_media_set_image = function() {
	                 var selection = wp.media.frames.gk_frame.state().get('selection');

	                 // no selection
	                 if (!selection) {
	                     return;
	                 }

	                 // iterate through selected elements
	                 selection.each(function(attachment) {
	                     var url = attachment.attributes.url;
	                     clicked_button.prev(selector).val(url);
	                 });
	             };

	             // closing event for media manger
	             wp.media.frames.gk_frame.on('close', gk_media_set_image);
	             // image selection event
	             wp.media.frames.gk_frame.on('select', gk_media_set_image);
	             // showing media manager
	             wp.media.frames.gk_frame.open();
	         });
	    });
		};


})( jQuery );
