<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Family_Medicine_Providers
 * @subpackage Canyon_View_Family_Medicine_Providers/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Canyon_View_Family_Medicine_Providers
 * @subpackage Canyon_View_Family_Medicine_Providers/admin
 * @author     Brandon West <brandon@mangledmonkeymedia.com>
 */
class Canyon_View_Family_Medicine_Providers_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Family_Medicine_Providers_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Family_Medicine_Providers_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/canyon-view-family-medicine-providers-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style('jquery-ui-css',  plugin_dir_url( __FILE__ ) . 'css/jquery-ui.min.css', array(), $this->version, 'all' );	

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Family_Medicine_Providers_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Family_Medicine_Providers_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/canyon-view-family-medicine-providers-admin.js', array( 'jquery' ), $this->version, false );

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/media-file-uploader.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'jquery-ui-datepicker' );
	}


	/**
	 * Add the profiles section to pages
	 *
	 * @since		1.0.0
	 */
	public function add_cvfmp_provider_meta_box() {
		global $post;
		$parentName = get_the_title($post->post_parent);

		if ( $parentName == "Providers") {
			add_meta_box(
				'cvfmp_provider_meta_box',
				__( 'Canyon View Family Medicine Provider', 'cvfmp_textdomain' ),
				array( $this, 'cvfmp_provider_meta_box_callback' ),
				'page'
			);
		}

	}

	/**
	 * Add the services section to pages
	 *
	 * @since		1.0.0
	 */
	public function add_cvfmp_services_meta_box() {
		global $post;
		$parentName = get_the_title($post->post_parent);

		if ( $parentName == "Services") {
			add_meta_box(
				'cvfmp_services_meta_box',
				__( 'Canyon View Family Medicine Provider', 'cvfmp_textdomain' ),
				array( $this, 'cvfmp_services_meta_box_callback' ),
				'page'
			);
		}

	}


	/**
	 * Returns a valid tagged URL for Services and Other Services
	 */
	function get_all_child_pages( $parentTitle )
	{
		$parent = get_page_by_title( $parentTitle );

		$args = array(
			'sort_order' => 'asc',
			'sort_column' => 'post_title',
			'hierarchical' => 1,
			'exclude' => '',
			'include' => '',
			'meta_key' => '',
			'meta_value' => '',
			'authors' => '',
			'child_of' => $parent->ID,
			'parent' => -1,
			'exclude_tree' => '',
			'number' => '',
			'offset' => 0,
			'post_type' => 'page',
			'post_status' => 'publish'
		);
		$pages = get_pages($args);

		return $pages;
	}


	/**
	 * Returns the total number of providers
	 * 
	 * Since 1.8.04
	 */
	function get_providers_count()
	{	
		return count(get_all_child_pages( 'Providers' ));
	}


	/**
	 * Returns a list of WordPress usernames
	 */
	function get_provider_user_names()
	{
		$args = array(
			'blog_id'      => $GLOBALS['blog_id'],
			'role'         => '',
			'role__in'     => array(),
			'role__not_in' => array(),
			'meta_key'     => '',
			'meta_value'   => '',
			'meta_compare' => '',
			'meta_query'   => array(),
			'date_query'   => array(),        
			'include'      => array(),
			'exclude'      => array(),
			'orderby'      => 'login',
			'order'        => 'ASC',
			'offset'       => '',
			'search'       => '',
			'number'       => '',
			'count_total'  => false,
			'fields'       => 'all',
			'who'          => ''
		); 
		$userlist = get_users( $args );

		return $userlist;
	}




// Providers Meta box

	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save_cvfmp_provider_meta_box( $post_id ) {

		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['cvfmp_meta_box_nonce'] ) )
			return $post_id;

		$nonce = $_POST['cvfmp_meta_box_nonce'];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'cvfmp_meta_box' ) )
			return $post_id;

		// If this is an autosave, our form has not been submitted,
                //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;

		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;
		}

		/* OK, its safe for us to save the data now. */

		// Sanitize the user input.
		$providerImageUrl = sanitize_text_field( $_POST['cvfmp_meta_provider_image_url'] );
		$providerVideoUrl = sanitize_text_field( $_POST['cvfmp_meta_provider_video_url'] );
		$providerVideoSplashUrl = sanitize_text_field( $_POST['cvfmp_meta_provider_video_splash_url'] );
		$providerNameFirst = sanitize_text_field( $_POST['cvfmp_meta_provider_name_first'] );
		$providerNameLast = sanitize_text_field( $_POST['cvfmp_meta_provider_name_last'] );
		$providerNameTitle = sanitize_text_field( $_POST['cvfmp_meta_provider_name_title'] );
		$providerGender= sanitize_text_field( $_POST['cvfmp_meta_provider_gender'] );
		$newProvider = sanitize_text_field( $_POST['cvfmp_meta_new_provider'] );
		$hideProvider = sanitize_text_field( $_POST['cvfmp_meta_hide_provider'] );
		$providerPhrase = sanitize_text_field( $_POST['cvfmp_meta_provider_phrase'] );
		$phoneNumber = sanitize_text_field( $_POST['cvfmp_meta_phone_number'] );
		$callPartner = sanitize_text_field( $_POST['cvfmp_meta_call_partner'] );
		$hideAppointmentsText = sanitize_text_field( $_POST['cvfmp_meta_hide_appointments_text'] );
		$altAppointmentsText =  $_POST['cvfmp_meta_alt_appointments_text'];
		$altCTA =  $_POST['cvfmp_meta_alt_cta'];
		$userName = sanitize_text_field( $_POST['cvfmp_meta_user_name'] );
		$googleReviewLocations = $_POST['cvfmp_meta_google_review_locations'];
		$googleReviewUrl = sanitize_text_field( $_POST['cvfmp_meta_google_review_url'] );
		$googleReviewPosition = sanitize_text_field( $_POST['cvfmp_meta_google_review_position'] );
		$enableGoogleReview = sanitize_text_field( $_POST['cvfmp_meta_enable_google_review'] );
		$providerRole = sanitize_text_field( $_POST['cvfmp_meta_provider_role'] );
		$acceptingNewPatients = sanitize_text_field( $_POST['cvfmp_meta_accepting_new_patients'] );
		$babiesDelivered = sanitize_text_field( $_POST['cvfmp_meta_babies_delivered'] );
		$babiesDeliveredTimestamp =  $_POST['cvfmp_meta_babies_delivered_timestamp'];
		$primarySpecialty = sanitize_text_field( $_POST['cvfmp_meta_primary_specialty'] );
		$medicalEducation = esc_textarea( $_POST['cvfmp_meta_medical_education'] );
		$internships = esc_textarea( $_POST['cvfmp_meta_internships'] );
		$residency = esc_textarea( $_POST['cvfmp_meta_residency'] );
		$fellowship = esc_textarea( $_POST['cvfmp_meta_fellowship'] );
		$boardCertifications = esc_textarea( $_POST['cvfmp_meta_board_certifications'] );
		$languages = esc_textarea( $_POST['cvfmp_meta_languages'] );
		$affiliations = esc_textarea( $_POST['cvfmp_meta_affiliations'] );
		$honorsAndAwards = sanitize_text_field( $_POST['cvfmp_meta_honors_and_awards'] );
		$biography = $_POST['cvfmp_meta_biography'];
		$biographyFoldCount = $_POST['cvfmp_meta_biography_fold_count'];
		$customFieldInfo = $_POST['cvfmp_meta_custom_field_info'];
		$customFieldBody = $_POST['cvfmp_meta_custom_field_body'];
		$candidPhotoLeftUrl = sanitize_text_field( $_POST['cvfmp_meta_candid_photo_left_url'] );
		$candidPhotoRightUrl = sanitize_text_field( $_POST['cvfmp_meta_candid_photo_right_url'] );
		$services = $_POST['cvfmp_meta_services'];
		$providerLocations = $_POST['cvfmp_meta_provider_locations'];

		// Update the meta field.
		update_post_meta( $post_id, '_cvfmp_meta_provider_image_url', $providerImageUrl);
		update_post_meta( $post_id, '_cvfmp_meta_provider_video_url', $providerVideoUrl);
		update_post_meta( $post_id, '_cvfmp_meta_provider_video_splash_url', $providerVideoSplashUrl);
		update_post_meta( $post_id, '_cvfmp_meta_provider_name_first', $providerNameFirst);
		update_post_meta( $post_id, '_cvfmp_meta_provider_name_last', $providerNameLast);
		update_post_meta( $post_id, '_cvfmp_meta_provider_name_title', $providerNameTitle);
		update_post_meta( $post_id, '_cvfmp_meta_provider_gender', $providerGender);
		update_post_meta( $post_id, '_cvfmp_meta_new_provider', $newProvider);
		update_post_meta( $post_id, '_cvfmp_meta_hide_provider', $hideProvider);
		update_post_meta( $post_id, '_cvfmp_meta_provider_phrase', $providerPhrase );
		update_post_meta( $post_id, '_cvfmp_meta_phone_number', $phoneNumber);
		update_post_meta( $post_id, '_cvfmp_meta_call_partner', $callPartner);
		update_post_meta( $post_id, '_cvfmp_meta_hide_appointments_text', $hideAppointmentsText);
		update_post_meta( $post_id, '_cvfmp_meta_alt_appointments_text', $altAppointmentsText);
		update_post_meta( $post_id, '_cvfmp_meta_alt_cta', $altCTA);
		update_post_meta( $post_id, '_cvfmp_meta_user_name', $userName);
		update_post_meta( $post_id, '_cvfmp_meta_google_review_locations', $googleReviewLocations);
		update_post_meta( $post_id, '_cvfmp_meta_google_review_url', $googleReviewUrl);
		update_post_meta( $post_id, '_cvfmp_meta_google_review_position', $googleReviewPosition);
		update_post_meta( $post_id, '_cvfmp_meta_enable_google_review', $enableGoogleReview);
		update_post_meta( $post_id, '_cvfmp_meta_provider_role', $providerRole);
		update_post_meta( $post_id, '_cvfmp_meta_accepting_new_patients', $acceptingNewPatients);
		update_post_meta( $post_id, '_cvfmp_meta_babies_delivered', $babiesDelivered);
		update_post_meta( $post_id, '_cvfmp_meta_babies_delivered_timestamp', $babiesDeliveredTimestamp);
		update_post_meta( $post_id, '_cvfmp_meta_primary_specialty', $primarySpecialty);
		update_post_meta( $post_id, '_cvfmp_meta_medical_education', $medicalEducation);
		update_post_meta( $post_id, '_cvfmp_meta_internships', $internships);
		update_post_meta( $post_id, '_cvfmp_meta_residency', $residency);
		update_post_meta( $post_id, '_cvfmp_meta_fellowship', $fellowship);
		update_post_meta( $post_id, '_cvfmp_meta_board_certifications', $boardCertifications);
		update_post_meta( $post_id, '_cvfmp_meta_languages', $languages);
		update_post_meta( $post_id, '_cvfmp_meta_affiliations', $affiliations);
		update_post_meta( $post_id, '_cvfmp_meta_honors_and_awards', $honorsAndAwards);
		update_post_meta( $post_id, '_cvfmp_meta_biography', $biography);
		update_post_meta( $post_id, '_cvfmp_meta_biography_fold_count', $biographyFoldCount);
		update_post_meta( $post_id, '_cvfmp_meta_custom_field_info', $customFieldInfo );
		update_post_meta( $post_id, '_cvfmp_meta_custom_field_body', $customFieldBody );
		update_post_meta( $post_id, '_cvfmp_meta_candid_photo_left_url', $candidPhotoLeftUrl);
		update_post_meta( $post_id, '_cvfmp_meta_candid_photo_right_url', $candidPhotoRightUrl);
		update_post_meta( $post_id, '_cvfmp_meta_services', $services);
		update_post_meta( $post_id, '_cvfmp_meta_provider_locations', $providerLocations);
	}

	/**
	 * Prints the box content.
	 *
	 * @param WP_Post $post The object for the current post/page.
	 */
	public function cvfmp_provider_meta_box_callback( $post ) {

		// Add a nonce field so we can check for it later.
		wp_nonce_field( 'cvfmp_meta_box', 'cvfmp_meta_box_nonce' );

		/*
		 * Use get_post_meta() to retrieve an existing value
		 * from the database and use the value for the form.
		 */
		$providerImageUrl = get_post_meta( $post->ID, '_cvfmp_meta_provider_image_url', true );
		$providerVideoUrl = get_post_meta( $post->ID, '_cvfmp_meta_provider_video_url', true );
		$providerVideoSplashUrl = get_post_meta( $post->ID, '_cvfmp_meta_provider_video_splash_url', true );
		$providerNameFirst = get_post_meta( $post->ID, '_cvfmp_meta_provider_name_first', true );
		$providerNameLast = get_post_meta( $post->ID, '_cvfmp_meta_provider_name_last', true );
		$providerNameTitle = get_post_meta( $post->ID, '_cvfmp_meta_provider_name_title', true );
		$providerGender = get_post_meta( $post->ID, '_cvfmp_meta_provider_gender', true );
		$newProvider = get_post_meta( $post->ID, '_cvfmp_meta_new_provider', true );
		$hideProvider = get_post_meta( $post->ID, '_cvfmp_meta_hide_provider', true );
		$providerPhrase = get_post_meta( $post->ID, '_cvfmp_meta_provider_phrase', true );
		$phoneNumber = get_post_meta( $post->ID, '_cvfmp_meta_phone_number', true );
		$callPartner = get_post_meta( $post->ID, '_cvfmp_meta_call_partner', true );
		$hideAppointmentsText = get_post_meta( $post->ID, '_cvfmp_meta_hide_appointments_text', true );
		$altAppointmentsText = get_post_meta( $post->ID, '_cvfmp_meta_alt_appointments_text', true );
		$altCTA = get_post_meta( $post->ID, '_cvfmp_meta_alt_cta', true );
		$userName = get_post_meta( $post->ID, '_cvfmp_meta_user_name', true );
		$googleReviewUrl = get_post_meta( $post->ID, '_cvfmp_meta_google_review_url', true );
		$googleReviewPosition = get_post_meta( $post->ID, '_cvfmp_meta_google_review_position', true );
		$enableGoogleReview = get_post_meta( $post->ID, '_cvfmp_meta_enable_google_review', true );
		$providerRole = get_post_meta( $post->ID, '_cvfmp_meta_provider_role', true );
		if ( empty( $providerRole ) ) { $providerRole = 'doctor'; }
		$acceptingNewPatients = get_post_meta( $post->ID, '_cvfmp_meta_accepting_new_patients', true );
		if ( empty( $acceptingNewPatients ) ) { $acceptingNewPatients = 'yes'; }
		$babiesDelivered = get_post_meta( $post->ID, '_cvfmp_meta_babies_delivered', true );
		if ( !isset( $babiesDelivered ) ) { $babiesDelivered = '0'; }
		$babiesDeliveredTimestamp = get_post_meta( $post->ID, '_cvfmp_meta_babies_delivered_timestamp', true );
		if ( !isset( $babiesDeliveredTimestamp ) ) { $babiesDeliveredTimestamp = current_time( 'mysql' ); }
		$primarySpecialty = get_post_meta( $post->ID, '_cvfmp_meta_primary_specialty', true );
		$medicalEducation = get_post_meta( $post->ID, '_cvfmp_meta_medical_education', true );
		$internships = get_post_meta( $post->ID, '_cvfmp_meta_internships', true );
		$residency = get_post_meta( $post->ID, '_cvfmp_meta_residency', true );
		$fellowship = get_post_meta( $post->ID, '_cvfmp_meta_fellowship', true );
		$boardCertifications = get_post_meta( $post->ID, '_cvfmp_meta_board_certifications', true );
		$languages = get_post_meta( $post->ID, '_cvfmp_meta_languages', true );
		$affiliations = get_post_meta( $post->ID, '_cvfmp_meta_affiliations', true );
		$honorsAndAwards = get_post_meta( $post->ID, '_cvfmp_meta_honors_and_awards', true );
		$biography = get_post_meta( $post->ID, '_cvfmp_meta_biography', true );
		$biographyFoldCount = get_post_meta( $post->ID, '_cvfmp_meta_biography_fold_count', true );
		if ( empty( $biographyFoldCount ) ) { $biographyFoldCount = '3'; }
		$customFieldInfo = get_post_meta( $post->ID, '_cvfmp_meta_custom_field_info', true );
		$customFieldBody = get_post_meta( $post->ID, '_cvfmp_meta_custom_field_body', true );
		$candidPhotoLeftUrl = get_post_meta( $post->ID, '_cvfmp_meta_candid_photo_left_url', true );
		$candidPhotoRightUrl = get_post_meta( $post->ID, '_cvfmp_meta_candid_photo_right_url', true );
		$services = get_post_meta( $post->ID, '_cvfmp_meta_services', true);
		is_array($services) ? : $services = [];
		$googleReviewLocations = get_post_meta( $post->ID, '_cvfmp_meta_google_review_locations', true);
		is_array($googleReviewLocations) ? : $googleReviewLocations = [];
		$providerLocations = get_post_meta( $post->ID, '_cvfmp_meta_provider_locations', true);
		is_array($providerLocations) ? : $providerLocations = [];

		$editorSettings = array(
  		'media_buttons' => false,
  		'tinymce' => array(
      	'theme_advanced_buttons1' => 'formatselect,|,bold,italic,underline,|,' .
        'bullist,blockquote,|,justifyleft,justifycenter' .
        ',justifyright,justifyfull,|,link,unlink,|' .
        ',spellchecker,wp_fullscreen,wp_adv'
  	));
	  	$locationsPage = get_page_by_title("Locations");
		$allLocations = get_pages( array(
			'parent'      => $locationsPage->ID,
			'hierarchical' => 0,
			'sort_column' => 'menu_order', 
			'sort_order' => 'asc'
		) );;
		$allServices = $this->get_all_child_pages("Services");
		$userList = $this->get_provider_user_names();
		$providers = $this->get_all_child_pages("Providers");
?>

		<div id="cvfmp-meta-box">

			<p>
				<label for="cvfmp-meta-provider-image-url" class="cvfmp-row-title">
					<?php _e( 'Provider Image', 'cvfmp_textdomain' ); ?>
				</label>
			<?php if ( $providerImageUrl != "" ){
					?>
					<img src="<?php echo esc_attr( $providerImageUrl ); ?>" id="cvfmp-meta-provider-image-img"  class="cvfmp-meta-provider-image"/>
				<?php
			} else { ?> <br> <?php } ?>
				<span class="url-label">Provider Image URL:</span>
				<input id="cvfmp-meta-provider-image-url" type="text" name="cvfmp_meta_provider_image_url" value="<?php echo esc_attr( $providerImageUrl ); ?>"/>
			  <input id="cvfmp_meta_provider_image_upload_button" type="button" class="button" value="Upload Image" />
			</p>
			<p>
				<label for="cvfmp-meta-provider-video-url" class="cvfmp-row-title">
					<?php _e( 'Provider Video URL', 'cvfmp_textdomain'); ?>
				</label>
				<input id="cvfmp-meta-provider-video-url" type="text" name="cvfmp_meta_provider_video_url" value="<?php echo esc_url( $providerVideoUrl ); ?>" size="50" />
			</p>
			<p>
				<label for="cvfmp-meta-provider-video-splash-url" class="cvfmp-row-title">
					<?php _e( 'Provider Video Splash URL', 'cvfmp_textdomain'); ?>
				</label>
				<input id="cvfmp-meta-provider-video-splash-url" type="text" name="cvfmp_meta_provider_video_splash_url" value="<?php echo esc_url( $providerVideoSplashUrl ); ?>" size="50" />
			</p>
			<p>
				<label for="cvfmp-meta-enable-google-review" class="cvfmp-row-title">
					<?php _e( 'Enable Single Page Google Review', 'cvfmp-textdomain');?>
				</label>
				<br>
				<label for="cvfmp_meta_enable_google_review_single_page">
					<input type="checkbox" id="cvfmp_meta_enable_google_review_single_page" name="cvfmp_meta_enable_google_review" class="cvfmp-meta-google-review-single-page" value="enableGoogleReview"<?php if ( $enableGoogleReview ) { echo " checked"; } ?>/>
				</label>
			</p>
			<p>
				<label for="cvfmp_meta_google_review_locations" class="cvfmp-row-title">
					<?php _e( 'Enable Google Review Locations', 'cvfmp-textdomain' ); ?>
				</label>
			<?php foreach ($allLocations as $location) {
				$locationPostTitle = $location->post_title;	?>
				<br>
				<label for="cvfmp_meta_google_review_locations_<?php echo $locationPostTitle ?>">
					<input type="checkbox" id="cvfmp_meta_google_review_locations_<?php echo $locationPostTitle ?>" name="cvfmp_meta_google_review_locations[]" class="cvfmp-meta-google-review-locations" value="<?php echo $locationPostTitle ?>" <?php if (in_array($locationPostTitle, $googleReviewLocations)) { echo 'checked="checked"'; } ?> /><?php echo $locationPostTitle ?>
				</label>
			<?php } ?>
			</p>
		 	<p>
				<label for="cvfmp-meta-google-review-url" class="cvfmp-row-title">
					<?php _e( 'Google Review URL', 'cvfmp_textdomain'); ?>
				</label>
				<input id="cvfmp-meta-google-review-url" type="text" name="cvfmp_meta_google_review_url" value="<?php echo esc_url( $googleReviewUrl ); ?>" size="50" />
			</p>
			<p>
				<label for="cvfmp-meta-google-review-position" class="cvfmp-row-title">
					<?php _e( 'Google Review Position', 'cvfmp_textdomain'); ?>
				</label><br>
				<select id="cvfmp-meta-google-review-position" type="text" name="cvfmp_meta_google_review_position">
					<?php for ($i = 0; $i <= count($providers); $i++) { ?>
						<option value="<?php echo $i; ?>" size="50"<?php if ( $i == $googleReviewPosition ) { echo ' selected'; } ?>>
							<?php echo $i; ?>
						</option>
					<?php } ?>
				</select>
			</p>
			<p>
				<label for="cvfmp-meta-provider-name-first" class="cvfmp-row-title">
					<?php _e( 'First Name', 'cvfmp_textdomain' ); ?>
				</label>
				<input type="text" id="cvfmp_meta_provider_name_first" name="cvfmp_meta_provider_name_first" value="<?php echo esc_attr( $providerNameFirst ) ?>" size="25" />
			</p>
			<p>
				<label for="cvfmp-meta-provider-name-last" class="cvfmp-row-title">
					<?php _e( 'Last Name', 'cvfmp_textdomain' ); ?>
				</label>
				<input type="text" id="cvfmp_meta_provider_name_last" name="cvfmp_meta_provider_name_last" value="<?php echo esc_attr( $providerNameLast ) ?>" size="25" />
			</p>
			<p>
				<label for="cvfmp-meta-provider-name-title" class="cvfmp-row-title">
					<?php _e( 'Title', 'cvfmp_textdomain' ); ?>
				</label>
				<span class="example">( ex. MD )</span>
				<input type="text" id="cvfmp_meta_provider_name_title" name="cvfmp_meta_provider_name_title" value="<?php echo esc_attr( $providerNameTitle ) ?>" size="10" maxlength="10"/>
			</p>
			<p>
				<label for="cvfmp-meta-provider-gender" class="cvfmp-row-title">
					<?php _e( 'Gender', 'cvfmp-textdomain');?>
				</label>
				<span class="radio-buttons">
					<input type="radio" id="cvfmp_meta_provider_gender" name="cvfmp_meta_provider_gender" class="cvfmp-meta-radio" value="male" <?php checked('male', $providerGender); ?> />Male<br>
					<input type="radio" id="cvfmp_meta_provider_gender" name="cvfmp_meta_provider_gender" class="cvfmp-meta-radio"  value="female" <?php checked('female', $providerGender); ?>/>Female
				</span>
			</p>
			<p>
				<label for="cvfmp-meta-new-provider" class="cvfmp-row-title">
					<?php _e( 'New provider?', 'cvfmp-textdomain');?>
				</label>
				<input type="checkbox" id="cvfmp_meta_new_provider" name="cvfmp_meta_new_provider" value="newProvider"<?php if ( $newProvider ) { echo " checked"; } ?>/>
			</p>
			<p>
				<label for="cvfmp-meta-hide-provider" class="cvfmp-row-title">
					<?php _e( 'Hide from Providers page?', 'cvfmp-textdomain');?>
				</label>
				<input type="checkbox" id="cvfmp_meta_hide_provider" name="cvfmp_meta_hide_provider" value="hideProvider"<?php if ( $hideProvider ) { echo " checked"; } ?>/>
			</p>
			<p>
				<label for="cvfmp-meta-provider-phrase" class="cvfmp-row-title">
					<?php _e( 'Provider Phrase', 'cvfmp_textdomain' ); ?>
				</label>
				<input type="text" id="cvfmp_meta_provider_phrase" name="cvfmp_meta_provider_phrase" value="<?php echo esc_attr( $providerPhrase ) ?>" size="25" />
			</p>
			<p>
				<label for="cvfmp-meta-phone-number" class="cvfmp-row-title">
					<?php _e( 'Phone Number', 'cvfmp-textdomain');?>
				</label>
				<span class="example">( ex. 555-555-5555 )</span>
				<input type="text" id="cvfmp_meta_phone_number" name="cvfmp_meta_phone_number" value="<?php echo esc_attr( $phoneNumber  );?>" size="12" maxlength="12"/>
			</p>
			<p>
				<label for="cvfmp-meta-call-partner" class="cvfmp-row-title">
					<?php _e( 'Call Partner', 'cvfmp-textdomain');?>
				</label>
				<input type="checkbox" id="cvfmp_meta_call_partner" name="cvfmp_meta_call_partner" value="callPartner"<?php if ( $callPartner ) { echo " checked"; } ?>/>
			</p>
			<p>
				<label for="cvfmp-meta-hide-appointments-text" class="cvfmp-row-title">
					<?php _e( 'Hide Appointments Text', 'cvfmp-textdomain');?>
				</label>
				<input type="checkbox" id="cvfmp_meta_hide_appointments_text" name="cvfmp_meta_hide_appointments_text" value="hideAppointmentsText"<?php if ( $hideAppointmentsText ) { echo " checked"; } ?>/>
			</p>
			<p>
				<label for="cvfmp-meta-alt-appointments-text" class="cvfmp-row-title">
					<?php _e( 'Alternate Appointments Text', 'cvfmp_textdomain' ); ?>
				</label>
				<?php wp_editor( stripslashes($altAppointmentsText), 'cvfmp_meta_alt_appointments_text', $editorSettings ); ?>
			</p>
			<p>
				<label for="cvfmp-meta-alt-cta" class="cvfmp-row-title">
					<?php _e( 'Alternate CTA', 'cvfmp_textdomain' ); ?>
				</label>
				<?php wp_editor( stripslashes($altCTA), 'cvfmp_meta_alt_cta', $editorSettings ); ?>
			</p>
			<p>
				<label for="cvfmp-meta-user-name" class="cvfmp-row-title">
					<?php _e( 'User Name', 'cvfmp-textdomain');?>
				</label><br />
				<select name="cvfmp_meta_user_name">
					<option value=""></option>
					<?php foreach ( $userList as $listName ) {?>
						<option value="<?php echo esc_attr( $listName->user_login ); ?>"<?php if ( $listName->user_login == $userName ) { echo " selected"; } ?> ><?php echo esc_attr( $listName->display_name );?> (<?php echo esc_attr( $listName->user_login ); ?>)</option>
					<?php } ?>
				</select>
     		</p>
			<p>
				<label for="cvfmp-meta-provider-role" class="cvfmp-row-title">
					<?php _e( 'Provider Role', 'cvfmp-textdomain');?>
				</label>
				<span class="radio-buttons">
					<input type="radio" id="cvfmp_meta_provider_role" name="cvfmp_meta_provider_role" class="cvfmp-meta-radio" value="doctor" <?php checked('doctor', $providerRole); ?> />Doctor<br>
					<input type="radio" id="cvfmp_meta_provider_role" name="cvfmp_meta_provider_role" class="cvfmp-meta-radio"  value="midlevel" <?php checked('midlevel', $providerRole); ?>/>Midlevel
				</span>
			</p>
			<p>
				<label for="cvfmp-meta-accepting-new-patients" class="cvfmp-row-title">
					<?php _e( 'Accepting New Patients?', 'cvfmp-textdomain');?>
				</label>
				<span class="radio-buttons">
					<input type="radio" id="cvfmp_meta_accepting_new_patients" name="cvfmp_meta_accepting_new_patients" class="cvfmp-meta-radio" value="yes" <?php checked('yes', $acceptingNewPatients); ?> />Yes<br>
					<input type="radio" id="cvfmp_meta_accepting_new_patients" name="cvfmp_meta_accepting_new_patients" class="cvfmp-meta-radio"  value="no" <?php checked('no', $acceptingNewPatients); ?>/>No<br>
					<input type="radio" id="cvfmp_meta_accepting_new_patients" name="cvfmp_meta_accepting_new_patients" class="cvfmp-meta-radio"  value="NA" <?php checked('NA', $acceptingNewPatients); ?>/>N/A
				</span>
			</p>
			<p>
				<label for="cvfmp-meta-babies-delivered" class="cvfmp-row-title">
					<?php _e( 'Babies Delivered', 'cvfmp-textdomain');?>
				</label>
				<span class="example">( 0 = disabled )</span>
				<input type="number" id="cvfmp_meta_babies_delivered" name="cvfmp_meta_babies_delivered" value="<?php echo esc_attr( $babiesDelivered ); ?>" size="3" maxlength="3" />
			</p>
			<p>
				<label for="cvfmp-meta-babies-delivered-timestamp" class="cvfmp-row-title">
					<?php _e( 'Last Baby Delivered Date', 'cvfmp-textdomain');?>
				<input type="text" class="custom_date" id="cvfmp_meta_babies_delivered_timestamp" name="cvfmp_meta_babies_delivered_timestamp" value="<?php echo  esc_attr( $babiesDeliveredTimestamp ); ?>" size="18" />
			</p>
			<p>
				<label for="cvfmp-meta-primary-specialty" class="cvfmp-row-title">
					<?php _e( 'Primary Specialty', 'cvfmp-textdomain');?>
				</label>
				<input type="text" id="cvfmp_meta_primary_specialty" name="cvfmp_meta_primary_specialty" value="<?php echo esc_attr( $primarySpecialty );?>" size="25" />
			</p>
			<p>
				<label for="cvfmp-meta-medical-education" class="cvfmp-row-title">
					<?php _e( 'Medical Education', 'cvfmp-textdomain');?>
				</label>
				<span class="example">( separate with ';' )</span>
			</label>
			<textarea id="cvfmp_meta_medical_education" name="cvfmp_meta_medical_education" rows="2" cols="50"><?php echo esc_attr( $medicalEducation );?></textarea>
			</p>
			<p>
				<label for="cvfmp-meta-internships" class="cvfmp-row-title">
					<?php _e( 'Internships', 'cvfmp-textdomain');?>
				</label>
				<span class="example">( separate with ';' )</span>
				<textarea id="cvfmp_meta_internships" name="cvfmp_meta_internships" rows="2" cols="50"><?php echo esc_attr( $internships );?></textarea>
			</p>
			<p>
				<label for="cvfmp-meta-residency" class="cvfmp-row-title">
					<?php _e( 'Residency', 'cvfmp-textdomain');?>
				</label>
				<span class="example">( separate with ';' )</span>
				<textarea id="cvfmp_meta_residency" name="cvfmp_meta_residency" rows="2" cols="50"><?php echo esc_attr( $residency );?></textarea>
			</p>
			<p>
				<label for="cvfmp-meta-fellowship" class="cvfmp-row-title">
					<?php _e( 'Fellowship', 'cvfmp-textdomain');?>
				</label>
				<span class="example">( separate with ';' )</span>
				<textarea id="cvfmp_meta_fellowship" name="cvfmp_meta_fellowship" rows="2" cols="50"><?php echo esc_attr( $fellowship );?></textarea>
			</p>
			<p>
				<label for="cvfmp-meta-board-certifications" class="cvfmp-row-title">
					<?php _e( 'Board Certifications', 'cvfmp-textdomain');?>
				</label>
				<span class="example">( separate with ';' )</span>
				<textarea id="cvfmp_meta_board_certifications" name="cvfmp_meta_board_certifications" rows="2" cols="50"><?php echo esc_attr( $boardCertifications );?></textarea>
			</p>
			<p>
				<label for="cvfmp-meta-languages" class="cvfmp-row-title">
					<?php _e( 'Languages other than English', 'cvfmp-textdomain');?>
				</label>
				<span class="example">( separate with ';' )</span>
				<textarea id="cvfmp_meta_languages" name="cvfmp_meta_languages" rows="2" cols="50"><?php echo esc_attr( $languages );?></textarea>
			</p>
			<p>
				<label for="cvfmp-meta-affiliations" class="cvfmp-row-title">
					<?php _e( 'Affiliations', 'cvfmp-textdomain');?>
				</label>
				<span class="example">( separate with ';' )</span>
				<textarea id="cvfmp_meta_affiliations" name="cvfmp_meta_affiliations" rows="2" cols="50"><?php echo esc_attr( $affiliations );?></textarea>
			</p>
			<p>
				<label for="cvfmp-meta-honors-and-awards" class="cvfmp-row-title">
					<?php _e( 'Honors and Awards', 'cvfmp-textdomain');?>
				</label>
				<span class="example">( separate with ';' )</span>
				<textarea id="cvfmp_meta_honors_and_awards" name="cvfmp_meta_honors_and_awards" rows="2" cols="50"><?php echo esc_attr( $honorsAndAwards );?></textarea>
			</p>
			<p>
				<label for="cvfmp-meta-biography-fold-count" class="cvfmp-row-title">
					<?php _e( 'Biography Fold Count', 'cvfmp-textdomain');?>
				</label>
				<span class="example">( 0 = no fold )</span>
				<input type="number" id="cvfmp_meta_biography_fold_count" name="cvfmp_meta_biography_fold_count" value="<?php echo esc_attr( $biographyFoldCount ); ?>" size="3" maxlength="3" />
			</p>
			<p>
				<label for="cvfmp-meta-biography" class="cvfmp-row-title">
					<?php _e( 'Biography', 'cvfmp-textdomain');?>
				</label>
				<?php wp_editor( stripslashes($biography), 'cvfmp_meta_biography', $editorSettings ); ?>
			</p>
			<p>
				<label for="cvfmp-meta-custom-field-info" class="cvfmp-row-title">
					<?php _e( 'Custom Field—Info', 'cvfmp-textdomain');?>
				</label>
				<?php wp_editor( stripslashes($customFieldInfo), 'cvfmp_meta_custom_field_info', $editorSettings ); ?>
			</p>
			<p>
				<label for="cvfmp-meta-custom-field-body" class="cvfmp-row-title">
					<?php _e( 'Custom Field—Body', 'cvfmp-textdomain');?>
				</label>
				<?php wp_editor( stripslashes($customFieldBody), 'cvfmp_meta_custom_field_body', $editorSettings ); ?>
			</p>
			<p>
				<label for="cvfmp-meta-candid-photo-left-url" class="cvfmp-row-title">
					<?php _e( 'Candid Photo Left', 'cvfmp_textdomain' ); ?>
				</label>
			<?php if ( $candidPhotoLeftUrl != "" ){
					?>
					<img src="<?php echo esc_attr( $candidPhotoLeftUrl); ?>" height="100" id="cvfmp-meta-candid-photo-left-img" class="cvfmp-meta-candid-photo"/>
				<?php
				}?>
				<span class="url-label">URL:</span>
				<input id="cvfmp-meta-candid-photo-left-url" type="text" name="cvfmp_meta_candid_photo_left_url" value="<?php echo esc_attr( $candidPhotoLeftUrl ); ?>"/>
			  <input id="cvfmp_meta_candid_photo_left_upload_button" type="button" class="button" value="Upload Image" />
			</p>
			<p>
				<label for="cvfmp-meta-candid-photo-right-url" class="cvfmp-row-title">
					<?php _e( 'Candid Photo Right', 'cvfmp_textdomain' ); ?>
				</label>
			<?php if ( $candidPhotoRightUrl != "" ){
					?>
					<img src="<?php echo esc_attr( $candidPhotoRightUrl ); ?>" height="100" id="cvfmp-meta-candid-photo-right-img" class="cvfmp-meta-candid-photo"/>
				<?php
				}?>
				<span class="url-label">URL:</span>
				<input id="cvfmp-meta-candid-photo-right-url" type="text" name="cvfmp_meta_candid_photo_right_url" value="<?php echo esc_attr( $candidPhotoRightUrl ); ?>"/>
			  <input id="cvfmp_meta_candid_photo_right_upload_button" type="button" class="button" value="Upload Image" />
			</p>
			<p>
				<label for="cvfmp-meta-services" class="cvfmp-row-title">
					<?php _e( 'Services', 'cvfmp-textdomain' ); ?>
				</label>
			<?php foreach ( $allServices as $service ) {
				$servicePostTitle = $service->post_title; ?>
				<br>
				<label for="cvfmp_meta_services_<?php echo $servicePostTitle ?>">
					<input type="checkbox" id="cvfmp_meta_services_<?php echo $servicePostTitle ?>" name="cvfmp_meta_services[]" class="cvfmp-meta-checkbox" value="<?php echo $servicePostTitle ?>" <?php if (in_array($servicePostTitle, $services)) { echo 'checked="checked"'; } ?> /><?php echo $servicePostTitle ?>
				</label>
			<?php } ?>
			</p>
			<p>
				<label for="cvfmp-meta-locations" class="cvfmp-row-title">
					<?php _e( 'Locations', 'cvfmp-textdomain' ); ?>
				</label>
			<?php foreach ($allLocations as $location) {
				$locationPostTitle = $location->post_title; ?>
				<br>
				<label for="cvfmp_meta_provider_locations_<?php echo $locationPostTitle ?>">
					<input type="checkbox" id="cvfmp_meta_provider_locations_<?php echo $locationPostTitle ?>" name="cvfmp_meta_provider_locations[]" class="cvfmp-meta-locations" value="<?php echo $locationPostTitle ?>" <?php if (in_array($locationPostTitle, $providerLocations)) { echo 'checked="checked"'; } ?> /><?php echo $locationPostTitle ?>
				</label>
			<?php } ?>
			</p>

		</div>

		<?php
	}


	// Services Meta Box

	/**
	 * Prints the meta box content.
	 *
	 * @param WP_Post $post The object for the current post/page.
	 */
	public function cvfmp_services_meta_box_callback( $post ) {

		// Add a nonce field so we can check for it later.
		wp_nonce_field( 'cvfmp_services_meta_box', 'cvfmp_services_meta_box_nonce' );

		/*
		 * Use get_post_meta() to retrieve an existing value
		 * from the database and use the value for the form.
		 */
		$serviceImageUrl = get_post_meta( $post->ID, '_cvfmp_meta_service_image_url', true );
		$serviceDetails = get_post_meta( $post->ID, '_cvfmp_meta_service_details', true );
		$serviceAdditionalResources = get_post_meta( $post->ID, '_cvfmp_meta_service_additional_resources', true );
		is_array($serviceAdditionalResources) ? : $serviceAdditionalResources = [];
		$serviceProvidersIntro = get_post_meta( $post->ID, '_cvfmp_meta_service_providers_intro', true );

		$editorSettings = array(
  		'media_buttons' => false,
  		'tinymce' => array(
      	'theme_advanced_buttons1' => 'formatselect,|,bold,italic,underline,|,' .
        'bullist,blockquote,|,justifyleft,justifycenter' .
        ',justifyright,justifyfull,|,link,unlink,|' .
        ',spellchecker,wp_fullscreen,wp_adv'
  	));

	  ?>
	  <div id="cvfmp-meta-box">

			<p>
				<label for="cvfmp-meta-service-image-url" class="cvfmp-row-title">
					<?php _e( 'Service Image', 'cvfmp_textdomain' ); ?>
				</label>
			<?php if ( $serviceImageUrl != "" ){
					?>
					<img src="<?php echo esc_attr( $serviceImageUrl ); ?>" height="200" id="cvfmp-meta-service-image-img"  class="cvfmp-meta-service-image"/>
				<?php
			} else { ?> <br> <?php }?>
				<span class="url-label">URL:</span>
				<input id="cvfmp-meta-service-image-url" type="text" name="cvfmp_meta_service_image_url" value="<?php echo esc_attr( $serviceImageUrl ); ?>"/>
			  <input id="cvfmp_meta_service_image_upload_button" type="button" class="button" value="Upload Image" />
			</p>
			<p>
				<label for="cvfmp-meta-service-details" class="cvfmp-row-title">
					<?php _e( 'Service Details', 'cvfmp-textdomain');?>
				</label>
				<?php wp_editor( stripslashes($serviceDetails), 'cvfmp_meta_service_details', $editorSettings ); ?>
			</p>
			<p>
				<label for="cvfmp-meta-service-additional-resources" class="cvfmp-row-title">
					<?php _e( 'Additional Resources', 'cvfmp-textdomain' ); ?>
				</label><br />
			<?php if (empty($serviceAdditionalResources)) { ?>
				<label>Title</label>
				<input type="text" id="cvfmp_meta_service_additional_resources" name="cvfmp_meta_service_additional_resources[]" value="" size="25" />
				<label>URL</label>
				<input type="text" id="cvfmp_meta_service_additional_resources" name="cvfmp_meta_service_additional_resources[]" value="" size="25" />
			<?php }	else {
			 	foreach ($serviceAdditionalResources as $resource) {	?>
					<input type="text" id="cvfmp_meta_service_additional_resources_" name="cvfmp_meta_meta_additional_resources[]" class="cvfmp-meta-service-additional-resources" value="<?php echo 'hi' ?>" <?php if (in_array($location->post_title, $providerLocations)) { echo 'checked="checked"'; } ?> /><?php echo $location->post_title ?><br />
			<?php }
				}?>
			</p>
			<p>
				<label for="cvfmp-meta-service-providers-intro" class="cvfmp-row-title">
					<?php _e( 'Service Providers Intro', 'cvfmp_textdomain' ); ?>
				</label>
				<input type="text" id="cvfmp_meta_service_providers_intro" name="cvfmp_meta_service_providers_intro" value="<?php echo esc_attr( $serviceProvidersIntro ) ?>" size="50" />
			</p>

	  </div>
	  <?php
	}


	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save_cvfmp_services_meta_box( $post_id ) {

		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['cvfmp_services_meta_box_nonce'] ) )
			return $post_id;

		$nonce = $_POST['cvfmp_services_meta_box_nonce'];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'cvfmp_services_meta_box' ) )
			return $post_id;

		// If this is an autosave, our form has not been submitted,
	            //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;

		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;
		}

		/* OK, its safe for us to save the data now. */

	  // Sanitize the user input.
	  $serviceImageUrl = sanitize_text_field( $_POST['cvfmp_meta_service_image_url'] );
		$serviceProvidersIntro = sanitize_text_field( $_POST['cvfmp_meta_service_providers_intro'] );
		$serviceAdditionalResources = $_POST['cvfmp_meta_service_additional_resources'];
		$serviceDetails = $_POST['cvfmp_meta_service_details'];

	  // Update the meta field.
		update_post_meta( $post_id, '_cvfmp_meta_service_image_url', $serviceImageUrl);
		update_post_meta( $post_id, '_cvfmp_meta_service_details', $serviceDetails);
		update_post_meta( $post_id, '_cvfmp_meta_service_additional_resources', $serviceAdditionalResources);
		update_post_meta( $post_id, '_cvfmp_meta_service_providers_intro', $serviceProvidersIntro);

	}

}
