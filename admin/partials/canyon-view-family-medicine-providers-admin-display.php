<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Family_Medicine_Providers
 * @subpackage Canyon_View_Family_Medicine_Providers/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
