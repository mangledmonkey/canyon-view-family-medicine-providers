<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Family_Medicine_Providers
 * @subpackage Canyon_View_Family_Medicine_Providers/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Canyon_View_Family_Medicine_Providers
 * @subpackage Canyon_View_Family_Medicine_Providers/includes
 * @author     Brandon West <brandon@mangledmonkeymedia.com>
 */
class Canyon_View_Family_Medicine_Providers_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
