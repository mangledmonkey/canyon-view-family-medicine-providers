<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Family_Medicine_Providers
 * @subpackage Canyon_View_Family_Medicine_Providers/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Canyon_View_Family_Medicine_Providers
 * @subpackage Canyon_View_Family_Medicine_Providers/public
 * @author     Brandon West <brandon@mangledmonkeymedia.com>
 */
class Canyon_View_Family_Medicine_Providers_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Family_Medicine_Providers_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Family_Medicine_Providers_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		// global $post;
		// $parent_title = get_the_title($post->post_parent);
		// if( is_page( array('providers') ) || ($parent_title == "Providers") || ($parent_title == "Locations" || ($parent_title == "Services") ) ){
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/canyon-view-family-medicine-providers-public.css', array(), $this->version, 'all' );
		// }

		

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Family_Medicine_Providers_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Family_Medicine_Providers_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/canyon-view-family-medicine-providers-public.js', array( 'jquery' ), $this->version, false );



	}

	/**
	 * Register the shortcodes for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function register_shortcodes() {

		add_shortcode( 'cvfmp_location_providers', array( $this, 'location_providers_shortcode' ) );
		add_shortcode( 'cvfmp_providers_list', array( $this, 'list_providers_shortcode' ) );
		add_shortcode( 'cvfmp_sorted_providers_list', array( $this, 'list_sorted_providers_shortcode' ) );
		add_shortcode( 'cvfmp_service_providers', array( $this, 'service_providers_shortcode' ) );
		add_shortcode( 'cvfmp_google_reviews', array( $this, 'google_reviews_shortcode' ) );

 	}

 /**
	 * Register REST route.
	 *
	 * @since    1.7.26
	 */
	public function register_provider_posts_api()
  {
    register_rest_route('cvfmp/v1', '/provider/(?P<slug>[a-zA-Z0-9-]+)/posts/(?P<limit>[0-9-]+)', array(
      'methods' => 'GET',
      'callback' => array($this, 'cvfmp_api_get_provider_posts_callback'),
    ) );
	}



	/**
	 * Returns an array of strings for the field
	 */
	public function list_provider_fields( $string )
	{
		$list = '';
		$items = explode(';', $string);

		foreach($items as $item) {
			$list .= '<li>' . $item . '</li>';
		}

		return $list;

	}

	/**
	 * Returns all providers
	 */
	function get_all_providers()
	{
		$parent = get_page_by_title("Providers");
		$args = array(
			'posts_per_page'   => -1,
			'offset'           => 0,
			'category'         => '',
			'category_name'    => '',
			'orderby'          => 'meta_value name',
			'order'            => 'ASC',
			'include'          => '',
			'exclude'          => '',
			'meta_key'         => '_cvfmp_meta_provider_name_last',
			'post_type'        => 'page',
			'post_mime_type'   => '',
			'post_parent'      => $parent->ID,
			'author'	   			 => '',
			'post_status'      => 'publish',
			'suppress_filters' => true
		);

		$providers = get_posts($args);

		return $providers;

	}



	/**
	 * Returns a full name string for a provider
	 */
 	function get_provider_full_name( $providerID )
	{
		$first = get_post_meta( $providerID, '_cvfmp_meta_provider_name_first', 1);
		$last = get_post_meta( $providerID, '_cvfmp_meta_provider_name_last', 1);
		$title = get_post_meta( $providerID, '_cvfmp_meta_provider_name_title', 1);

	  $fullname = $first . ' ' . $last;
		
		if ( $title != '' ) {
			$fullname .= ', ' . $title;
		}

		return $fullname;
	}

	/**
	 * Returns html elements for provider page listing
	 */
	function build_provider_list_item( $provider )
	{
		$name = $provider[0];
		$image = $provider[1];
		$phone = $provider[2];
		$url = $provider[3];
		$new = $provider[5];

		$html = '
			<div class="provider-listing-cell">
				<div class="provider-listing" style="background-image:url(\'' . esc_attr( $image ) . '\');">';

		if ( $new != '' ) {
			$html .= '
				 <div class="new-provider-banner">New Provider</div>';
		}

		$html .= '
		    	<a href="' . esc_attr( $url ) . '">&nbsp;</a>
		    	<div>
						<h3><a href="' . esc_attr( $url ) . '">' . esc_attr( $name ) . '</a></h3>';

		if ( $phone != '' ) {
	    $html .= '
				    <a href="tel:+1-' . esc_attr( $phone ) . '">Call: ' . esc_attr( $phone ) . '</a>';
		} else {
			$html .= '
				    <a href="'. esc_attr( $url ) . '">View profile</a>';
		}

		$html .='
					</div>
	    	</div>
			</div>';

		return $html;

	}


	/**
	 * Returns a single provider user data object
   * 
   * @since    1.7.26
	 */
  function cvfmp_api_get_provider_posts_callback( $data ) {
		$limit = $data['limit'];
		$posts = [];
		$provider_posts = [];

    $user = get_user_by('slug', $data['slug']);
    // $provider_data = [[
    //   'id' => $user->ID
    // ]];

		if ( $user ) {
			$post_args = array(
				'author' 		=> $user->ID,
				'numberposts' 	=> $limit,
				'post_status' 	=> 'publish',
				'orderby'   => 'date',
				'order'     => 'desc',
				'post_type' => 'post',
			);

			$posts = get_posts( $post_args );
		}

		foreach ($posts as $post):
			$post_data = [
				'link' => get_permalink($post->ID),
				'title' => $post->post_title,
				'featured_image_src' => get_the_post_thumbnail_url($post->ID),
			];
			array_push($provider_posts, $post_data);
		endforeach;

    return $provider_posts;
  }

	/**
	 * Returns a full name string for a provider
	 */
 	function get_provider_posts( $post_author, $limit = -1 )
	{
		$posts = [];
		$remote = 'https://canyonviewmedical.com/wp-json/cvfmp/v1/provider/' . $post_author . '/posts/' . $limit;

		if ( $remote !== '' ) {
			$request = wp_remote_get( $remote );

			if( is_wp_error( $request ) ) {
				return false; // Bail early
			}
			
			$body = wp_remote_retrieve_body( $request );

			return json_decode( $body, true );
		}

		return $posts;
	}


	/**
	 * Returns all providers at a location
	 */
	function get_related_providers( $title, $meta_key )
	{
		$providers = $this->get_all_providers();

		$relatedProviders = [];
		$count = 0;

		foreach ($providers as $provider)
		{
			$relations = get_post_meta( $provider->ID, $meta_key, 1);
			if ( !is_array($relations) ) { $relations = []; }


			if (in_array( $title, $relations )) {
				// $image = get_post_meta( $provider->ID, '_cvfmp_meta_provider_image_url', 1);
				// $url = get_page_link( $provider->ID );
				// $fullname = $this->get_provider_full_name( $provider->ID );

				// $relatedProviders[$count] = array( $fullname, $image, $url);
				$relatedProviders[$count] = $provider;

				$count ++;

			}
		}

		return $relatedProviders;
	}

	/**
	 * sorts and separates doctors and midlevels returning 2 arrays
	 *
	 * @since    1.0.2
	 */
	 function get_sorted_providers( $title = '', $meta_key = '' ) {
		// if no providers are passed, get all providers
		if ( $title == '' && $meta_key == '' ) {
			$providers = $this->get_all_providers();
		} else {
			$providers = $this->get_related_providers( $title, $meta_key );
		}

		$doctors = [];
		$midlevels = [];

		$doctorsCount = 0;
		$midlevelsCount = 0;

		foreach( $providers as $provider )
		{
			$providerRole = get_post_meta( $provider->ID, '_cvfmp_meta_provider_role', 1);
			$name = $this->get_provider_full_name( $provider->ID );
			$image = get_post_meta( $provider->ID, '_cvfmp_meta_provider_image_url', 1);
			$phone = get_post_meta( $provider->ID, '_cvfmp_meta_phone_number', 1);
			$url = get_page_link( $provider->ID );
			$hide = get_post_meta ( $provider->ID, '_cvfmp_meta_hide_provider', 1);
			$details = array( $name, $image, $phone, $url, $hide);

			if ( $providerRole == 'doctor' ) {
				$doctors[$doctorsCount] = $details;
				$doctorsCount ++;
			} elseif ( $providerRole == 'midlevel' ) {
				$midlevels[$midlevelsCount] = $details;
				$midlevelsCount ++;
			}
		}

		return array( $doctors, $midlevels );
	 }

	 /**
	 * Collects providers and midlevels and sorts all together alphabetically
	 *
	 * @since    1.3.0
	 */
	function get_alpha_providers( $title = '', $meta_key = '' ) {
		// if no providers are passed, get all providers
		if ( $title == '' && $meta_key == '' ) {
			$providers = $this->get_all_providers();
		} else {
			$providers = $this->get_related_providers( $title, $meta_key );
		}

		$allProviders = [];
		$allProvidersCount = 0;

		foreach( $providers as $provider )
		{
			$providerRole = get_post_meta( $provider->ID, '_cvfmp_meta_provider_role', 1);
			$name = $this->get_provider_full_name( $provider->ID );
			$image = get_post_meta( $provider->ID, '_cvfmp_meta_provider_image_url', 1);
			$phone = get_post_meta( $provider->ID, '_cvfmp_meta_phone_number', 1);
			$url = get_page_link( $provider->ID );
			$hide = get_post_meta ( $provider->ID, '_cvfmp_meta_hide_provider', 1);
			$new = get_post_meta( $provider->ID, '_cvfmp_meta_new_provider', 1);
			$details = array( $name, $image, $phone, $url, $hide, $new);

			$allProviders[$allProvidersCount] = $details;
			$allProvidersCount ++;
			
		}

		return $allProviders;
	 }

	/**
	 * Returns html elements for provider on service page
	 */
	function build_service_provider_list_item( $provider, $large = false )
	{
		$name = $provider[0];
		$image = $provider[1];
		$url = $provider[3];

		if ( $large ) {
			$classLarge = ' large';
		}

		$html = '
					<a href="'. $url . '" title="' . $name . '" class="provider-listing' . $classLarge . '">
						<img src="' . $image . '" alt="' . $name . '" style="opacity:1;"/>
						' . $name . '
					</a>';

		return $html;

	}

	


	/**
	 * Prints the provider met info to live page.
	 *
	 * @param WP_Post $post The object for the current post/page.
	 */
	public function insert_cvfmp_provider( $content )
	{
		global $post;
		$parentName = get_the_title($post->post_parent);

    // Add to page content if parent page is "Providers"
		if ( $parentName == "Providers" && $post->post_title != "Providers" ) {

			$providerImageUrl = get_post_meta( $post->ID, '_cvfmp_meta_provider_image_url', 1);
			$providerVideoUrl = get_post_meta( $post->ID, '_cvfmp_meta_provider_video_url', 1);
			$providerVideoSplashUrl = get_post_meta( $post->ID, '_cvfmp_meta_provider_video_splash_url', 1);
			$providerVideoEmbed = get_post_meta( $post->ID, '_cvfmp_meta_provider_video_embed', 1);
			$providerPhrase = get_post_meta( $post->ID, '_cvfmp_meta_provider_phrase', 1);
			$phoneNumber = get_post_meta( $post->ID, '_cvfmp_meta_phone_number', 1);
			$hideAppointmentsText = get_post_meta( $post->ID, '_cvfmp_meta_hide_appointments_text', 1);
			$altAppointmentsText = get_post_meta( $post->ID, '_cvfmp_meta_alt_appointments_text', 1);
			$altCTA = get_post_meta( $post->ID, '_cvfmp_meta_alt_cta', 1);
			$acceptingNewPatients = get_post_meta( $post->ID, '_cvfmp_meta_accepting_new_patients', 1);
			$babiesDelivered = get_post_meta( $post->ID, '_cvfmp_meta_babies_delivered', 1);
			$babiesDeliveredTimestamp = get_post_meta( $post->ID, '_cvfmp_meta_babies_delivered_timestamp', 1);
			$primarySpecialty = get_post_meta( $post->ID, '_cvfmp_meta_primary_specialty', 1);
			$medicalEducation = get_post_meta( $post->ID, '_cvfmp_meta_medical_education', 1);
			$internships = get_post_meta( $post->ID, '_cvfmp_meta_internships', 1);
			$boardCertifications = get_post_meta( $post->ID, '_cvfmp_meta_board_certifications', 1);
			$residency = get_post_meta( $post->ID, '_cvfmp_meta_residency', 1);
			$fellowship = get_post_meta( $post->ID, '_cvfmp_meta_fellowship', 1);
			$languages = get_post_meta( $post->ID, '_cvfmp_meta_languages', 1);
			$affiliations = get_post_meta( $post->ID, '_cvfmp_meta_affiliations', 1);
			$honorsAndAwards = get_post_meta( $post->ID, '_cvfmp_meta_honors_and_awards', 1);
			$biography = get_post_meta( $post->ID, '_cvfmp_meta_biography', 1);
			$biographyFoldCount = get_post_meta( $post->ID, '_cvfmp_meta_biography_fold_count', 1);
			if ( empty( $biographyFoldCount ) ) { $biographyFoldCount = '3'; }
			$customFieldInfo = get_post_meta( $post->ID, '_cvfmp_meta_custom_field_info', 1);
			$customFieldBody = get_post_meta( $post->ID, '_cvfmp_meta_custom_field_body', 1);
			$candidPhotoLeft = get_post_meta( $post->ID, '_cvfmp_meta_candid_photo_left_url', 1);
			$candidPhotoRight = get_post_meta( $post->ID, '_cvfmp_meta_candid_photo_right_url', 1);
			$services = get_post_meta( $post->ID, '_cvfmp_meta_services', 1);
			is_array($services)? : $services = [];
			$otherServices = get_post_meta( $post->ID, '_cvfmp_meta_other_services', 1);
			is_array($otherServices)? : $otherServices = [];
			$providerLocations = get_post_meta( $post->ID, '_cvfmp_meta_provider_locations', 1);
			$postUserName = get_post_meta ( $post->ID, '_cvfmp_meta_user_name', 1);
			$callPartner = get_post_meta ( $post->ID, '_cvfmp_meta_call_partner', 1);

			$providerName = $this->get_provider_full_name( $post->ID );

			$providerContent = '
				<article id="provider">
			    <div class="profileInfo">
		        <img src="' . esc_attr( $providerImageUrl ) . '" alt="' . esc_attr( $providerName ) . '" />';

		  if ( $phoneNumber != '' ) {
				if ( $callPartner ) {
			 		$providerContent .= '
		 		    <a class="phoneCta" href="tel:+1-' . esc_attr( $phoneNumber ) . '" title="Call today to make an appointment with one of my partners">
		            <span>' . esc_attr( $providerName ) . '</span>
			        <span>For an appointment with one of my partners, call:</span>
		            <span>' . esc_attr( $phoneNumber ) . '</span>';
	
				} else if ( !empty($altCTA) ) {
					$providerContent .= '
					<a class="phoneCta" href="tel:+1-' . esc_attr( $phoneNumber ) . '" title="Call today to make an appointment with ' . esc_attr( $providerName ) . '">
				    <span>' . esc_attr( $providerName ) . '</span>
					<span>' . str_replace(['<p>', '</p>'], '', wpautop( $altCTA ) ) . '</span>
				    <span>' . esc_attr( $phoneNumber ) . '</span>';
				} else {
			 		$providerContent .= '
		 		    <a class="phoneCta" href="tel:+1-' . esc_attr( $phoneNumber ) . '" title="Call today to make an appointment with ' . esc_attr( $providerName ) . '">
		            <span>' . esc_attr( $providerName ) . '</span>
			        <span>Call for an appointment:</span>
		            <span>' . esc_attr( $phoneNumber ) . '</span>';
				}
			} else {
				$providerContent .= '
		        <a class="phoneCta" href="#" title="' . esc_attr( $providerName ) . ' is not taking appointments at this time.">
		            <span>' . esc_attr( $providerName ) . '</span>';
			}

				$providerContent .= '		
		        </a>';

			// Babies Delivered counter
			if ( $babiesDelivered > 0 ) {
				$providerContent .= '
						<div id="babyCounter">
					    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				         	viewBox="-404 206 150 150" style="enable-background:new -404 206 150 150;" xml:space="preserve">
				        <style type="text/css">
			            .st10{fill:#12528C;}
			            .st11{fill:#8A2575;}
			            .st12{fill:#FAB589;}
			            .st13{fill:#3C2415;}
			            .st14{fill:#D66CAA;}
			            .st15{fill:#A3DCE6;}
				        </style>
				        <g>
			            <g>
		                <path class="st10" d="M-333.4,319.4c-1.1-10.4,27.6-25.4,37.4-37.4c9.8-12,3.6-31.2,0-36.3c-3.3-4.8-35.2-24-51.6-36.2
		                	c-31.8,8.3-55.3,37.1-55.3,71.6c0,38.8,29.9,70.5,67.8,73.6C-336.5,341-332.4,328.7-333.4,319.4z"/>
		                <path class="st11" d="M-329,207.1c-6.4,0-12.6,0.8-18.6,2.3c22,29.3,42.6,27.1,46,32c3.6,5.1,15.6,28.5,5.8,40.5
		                  c-9.8,12-38.5,27-37.4,37.4c0.9,9.4-3.3,21.8-1.7,35.2c2,0.2,3.9,0.3,5.9,0.3c40.8,0,73.9-33.1,73.9-73.9
		                  C-255.3,240.2-288.3,207.1-329,207.1z"/>
				            </g>
				            <ellipse class="st12" cx="-324.8" cy="264.2" rx="24.9" ry="25.1"/>
				            <ellipse class="st13" cx="-316.8" cy="264" rx="3" ry="3.9"/>
				            <ellipse class="st13" cx="-333.1" cy="264" rx="3" ry="3.9"/>
				            <g>
			                <path class="st14" d="M-324.8,278.8c-2.7,0-5-1.4-6.2-3.7c-0.2-0.2,0-0.5,0.2-0.6c0.2-0.2,0.5,0,0.6,0.2c1.1,2,3.1,3.3,5.5,3.3
			                    c2.3,0,4.4-1.2,5.5-3.3c0.2-0.2,0.3-0.3,0.6-0.2c0.2,0.2,0.3,0.3,0.2,0.6C-319.8,277.4-322.1,278.8-324.8,278.8z"/>
			            	</g>
					          <g>
			                <path class="st15" d="M-336.2,309.4c5.1-12.5,17.3-16.8,10.8-16.5c-25.1,1.1-28.7-24.2-28.7-29.3c0-16.1,7.5-29,28.7-29
		                    c0.5,0,0.8,0,1.2,0c-2.2-1.2-5.3-3-10-4.7c-15.9,2.2-27.9,12.5-28.4,35.2c-0.5,23.2,5.3,65.3,25.4,75.3
		                    C-338,334-340.1,319-336.2,309.4z"/>
			                <path class="st14" d="M-325.9,229.4c-0.6,0-1.1,0-1.7,0c2.2,1.6,6.2,3.9,7.5,5.5c14.5,1.6,23.4,13.7,23.4,28.7
		                    c-0.5,19.2-10.6,22-18.7,28.4c-4.2,3.3-14.3,11.4-17.6,22.6c-1.9,6.2-3.1,19.3-2.8,26.2c2.5,1.2,4.5,1.7,7.6,1.7
		                    c27.3,0.6,36.8-49.6,37.4-76.1C-290.2,240-306.1,229.7-325.9,229.4z"/>
				            </g>
					        </g>
					    	</svg>
					    <h6>Babies Delivered</h6>
					    <span>' . esc_attr( $babiesDelivered ) . '</span>
							<span class="asof">as of: ' . $babiesDeliveredTimestamp . '</span>
						</div>';
			}

			// Custom Field Body
			if ( isset( $customFieldInfo ) ) {
				$providerContent .= wpautop(stripcslashes($customFieldInfo));
			}

			// end div .profileInfo
			$providerContent .= '
					</div>';

			$providerContent .= '
			    <section class="bio">';

			$providerContent .= '
						<h2>' . esc_attr( $providerPhrase ) . '</h2>';
			
			$providerContent .= '
						<div id="biography">
							<div class="read-more" fold-count="' . esc_attr( $biographyFoldCount ) . '">
								' . wpautop(stripslashes($biography)) . '
							</div>
							<div class="read-more-text">
								<a href="" class="read-more-link">Show more ▼</a>
							</div>
						</div>';
		
			// Provider Video
			if ( !empty( $providerVideoUrl ) ) {
				if ( strpos($providerVideoUrl, 'vimeo') !== false ) {
					$providerContent .= '
						<div id="providerVideo">
							<div id="providerVideoEmbed">Loading video...</div>
						</div>
						<script type="application/javascript">
							// Provider Video Embed
							var videoWidth = Math.round(document.getElementsByClassName(\'bio\')[0].offsetWidth * 0.8);
							var videoUrl = \'' . $providerVideoUrl . '\';
							var endpoint = \'https://www.vimeo.com/api/oembed.json\';
							var callback = \'embedVideo\';
							var url = endpoint + \'?url=\' + encodeURIComponent(videoUrl) + \'&callback=\'+ callback + \'&width=\' + videoWidth;

							function embedVideo(video) {
								document.getElementById(\'providerVideoEmbed\').innerHTML = unescape(video.html);
							}

							function init() {
								var js = document.createElement(\'script\');
								js.setAttribute(\'type\', \'text/javascript\');
								js.setAttribute(\'src\', url);
								document.getElementsByTagName(\'head\').item(0).appendChild(js);
							}
							window.onload = init;
						</script>';
				} else {
					if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $providerVideoUrl, $match)) {
						$video_id = $match[1];
					}
					
					$providerContent .= '
						<div id="ytplayer"></div>
						<script>
							// Load the IFrame Player API code asynchronously.
							var tag = document.createElement(\'script\');
							tag.src = "https://www.youtube.com/player_api";
							var firstScriptTag = document.getElementsByTagName(\'script\')[0];
							firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

							// Replace the \'ytplayer\' element with an <iframe> and
							// YouTube player after the API code downloads.
							var player;
							function onYouTubePlayerAPIReady() {
								player = new YT.Player(\'ytplayer\', {
									height: \'360\',
									width: \'640\',
									videoId: \'' . $video_id . '\'
								});
							}
						</script>';
				}
			}
						
			if ( $phoneNumber && !$callPartner && !$hideAppointmentsText ) {
				if ( !empty($altAppointmentsText) ) {
					$providerContent .= '
		      			<p class="bioCta">' . wpautop( $altAppointmentsText ) . '</p>';
				} else {
					$providerContent .= '
		      			<p class="bioCta">For appointments with ' . esc_attr( $providerName ) . ' call <a href="tel:+1-' . esc_attr( $phoneNumber ) . '">' . esc_attr( $phoneNumber) . '</a>.</p>';
				}
			}
			
			// Provider Candid Photos
			if ( $candidPhotoLeft ) {
			  $providerContent .= '
						<img class="candid" src="' . esc_attr( $candidPhotoLeft ) . '" />';
			}
			if ( $candidPhotoRight ) {
			  $providerContent .= '
						<img class="candid" src="' . esc_attr( $candidPhotoRight ) . '" />';
			}

		  // Provider Locations
      if ( $providerLocations ) {
      $providerContent .= '
						<p><strong>' . esc_attr( $providerName ) . ' provides services at:</strong></p>';
				foreach($providerLocations as $locationTitle) 
				{
					$location = get_page_by_title( $locationTitle );
					$locationThumbnail = get_the_post_thumbnail_url( $location->ID );

					$providerContent .= '
							<a href="'. get_permalink( $location ) . '" class="location" title="More information about Canyon View Family Medicine in ' . $location->post_title . '">
									<h5 class="greenGrad">' . $locationTitle . '</h5>
									<img src="' . $locationThumbnail . '" class="providerLoc">
							</a>';
				}
			}

			// Custom Field Body
			if ( isset( $customFieldBody ) ) {
				$providerContent .= wpautop(stripcslashes($customFieldBody));
			}

			$providerContent .= '
			    </section>';



			// Provider posts as stories
			if ( $postUserName ) {
				$postsLimit = 5;
				$userPosts = $this->get_provider_posts( $postUserName, $postsLimit );

				if ( !empty($userPosts) ) {
					$provider = get_user_by( 'login', $postUserName );
					$postsDomain = 'https://canyonviewmedical.com';
					$userPostsUrl = $postsDomain . '/?s=&post_type[]=post&post_author=' . $postUserName;

					$stories = '
							<section class="stories">
								<strong>Stories by ' . esc_attr( $providerName ) . ':</strong>';

					foreach ( $userPosts as $userPost ) {
						$postTitle = $userPost['title'];
						$postLink =  $postsDomain . $userPost['link'];
						$postThumbnailUrl = $postsDomain . $userPost['featured_image_src'];

						$stories .= '
							<a class="providerStory" href="' . $postLink . '" target="_blank">
									<span class="storyThumb" style="background-image: url(' . $postThumbnailUrl . ');">&nbsp;</span>
									<span class="storyTitle">' . $postTitle . '</span>
							</a>';
					}

					if ( count($userPosts) === $postsLimit ) {
						$stories .= '
							<div class="see-more-posts">
								<a href="' . $userPostsUrl . '" class="" target="_blank">See more stories. . .</a>
							</div>';
					}

					$stories .= '
						</section>';

					$providerContent .= $stories;
				}
			}


			//  Provider services
			if (!empty($services)) {
				$providerContent .= '
			    <section class="services">
			        <h3>Services provided by ' . esc_attr( $providerName ) . '</h3>
			        <ul>';

				foreach($services as $service) {
					$page = get_page_by_title( $service );
					$url = get_page_link( $page->ID );

					$providerContent .= '
			            <li><a href="'. esc_attr ( $url ) . '">' . esc_attr( $page->post_title ) . '</a></li>';
				}

				$providerContent .= '
			        </ul>



			    </section>';
			}

				$providerContent .= '
			    <section class="additionalInfo">
						<div class="infoContent">';

			if ( $acceptingNewPatients != 'NA') {
				$providerContent .= '
							<ul>
									<h4>Accepting new patients:</h4>
									<li>I am ' . ($acceptingNewPatients == 'yes' ? '': 'not ' ) . 'accepting new patients.</li>
							</ul>';
			}

			if ( $primarySpecialty != '' ) {
				$providerContent .= '
							<ul>
								<h4>Primary Specialty:</h4>
								' . $this->list_provider_fields( $primarySpecialty ) . '
							</ul>';
			}
			if ( $medicalEducation != '' ) {
		  	$providerContent .= '
							<ul>
								<h4>Medical Education:</h4>
								' . $this->list_provider_fields( $medicalEducation ) . '
							</ul>';
			}
			if ( $internships != '' ) {
				$providerContent .= '
							<ul>
								<h4>Internships:</h4>
								' . $this->list_provider_fields( $internships ) . '
							</ul>';
			}
			if ( $residency != '' ) {
				$providerContent .= '
							<ul>
								<h4>Residency:</h4>
								' . $this->list_provider_fields( $residency ) . '
							</ul>';
			}
			if ( $fellowship != '' ) {
				$providerContent .= '
							<ul>
								<h4>Fellowship:</h4>
								' . $this->list_provider_fields( $fellowship ) . '
							</ul>';
			}
			if ( $boardCertifications != '' ) {
				$providerContent .= '
							<ul>
								<h4>Board Certifications:</h4>
								' . $this->list_provider_fields( $boardCertifications ) . '
							</ul>';
			}
			if ( $languages != '' ) {
				$providerContent .= '
							<ul>
								<h4>Languages other than English:</h4>
								' . $this->list_provider_fields( $languages ) . '
							</ul>';
			}
			if ( $affiliations != '' ) {
				$providerContent .= '
							<ul>
								<h4>Affiliations:</h4>
								' . $this->list_provider_fields( $affiliations ) . '
							</ul>';
			}
			if ( $honorsAndAwards != '' ) {
				$providerContent .= '
							<ul>
								<h4>Honors and Awards:</h4>
								' . $this->list_provider_fields( $honorsAndAwards ) . '
							</ul>';
			}

		  		$providerContent .= '
					</div>
				</section>
			</article>

			';


	        $content .= $providerContent;

			}
    return $content;
	}

	/**
	 * shortcode [cvfmp_location_providers]
	 *
	 * @since    1.0.0
	 */
	public function location_providers_shortcode() {
		global $post;

		$providers = $this->get_alpha_providers( $post->post_title, '_cvfmp_meta_provider_locations' );

 		$html = '
 		<section id="location-providers">
			<h4>Healthcare Providers Serving ' . $post->post_title . '</h4>
			<div class="doctors">';

			foreach ( $providers as $provider ) {
				$html .= $this->build_service_provider_list_item( $provider );
			}

			$html .= '
			</div>
		</section>';

		return $html;

	}

	/**
	 * shortcode [cvfmp_sorted_providers_list]
	 *
	 * @since    1.0.0
	 */
	public function list_sorted_providers_shortcode() {
		list ( $doctors, $midlevels ) = $this->get_sorted_providers();

		$html = '
			<section id="providers">
				<div class="doctors">
			';

		foreach ( $doctors as $doctor ) {
			if ( !$doctor[4] ) {
				$html .= $this->build_provider_list_item( $doctor );
			}
		}

		$html .= '
				</div>
				<div class="midlevels">';

		foreach ( $midlevels as $midlevel ) {
			if ( !$doctor[4] ) {
				$html .= $this->build_provider_list_item( $midlevel );
			}
		}

		$html .= '
				</div>
			</section>';

			return $html;

	}

	/**
	 * shortcode [cvfmp_providers_list]
	 *
	 * @since    1.3.0
	 */
	public function list_providers_shortcode() {
		$providers = $this->get_alpha_providers();

		$html = '
			<section id="providers">
				<div id="provider-listing">
			';

		foreach ( $providers as $provider ) {
			if ( !$provider[4] ) {	
				$html .= $this->build_provider_list_item( $provider );
			}
		}

		$html .= '
				</div>
			</section>';

			return $html;

	}

 	/**
 	 * shortcode [cvfmp_service_providers title]
 	 *
 	 * @since    1.0.0
 	 */
 	public function service_providers_shortcode( $atts ) {
 		global $post;
		$a = shortcode_atts( array(
			'title' => 'Contact a provider to schedule an appointment.'
		), $atts );
		$large = false;
		$providers = $this->get_alpha_providers( $post->post_title, '_cvfmp_meta_services' );

 		$html = '
 		<section id="service-providers">
 			<h4>' . $a['title'] . '</h4>
			<div class="doctors">';

		foreach ( $providers as $provider ) {
			$html .= $this->build_service_provider_list_item( $provider, $large );
		}

 		$html .= '
 			</div>
 		</section>';
 		
 		return $html;

 	}


	/**
	 * Returns the HTML for Google Review list items
	 *
	 * @since    1.8.04
	 */
	private function create_google_reviews_item( $provider ) {
		$providerName = $this->get_provider_full_name( $provider->ID );
		$providerImageUrl = get_post_meta( $provider->ID, '_cvfmp_meta_provider_image_url', 1);
		$googleReviewUrl = get_post_meta( $provider->ID, '_cvfmp_meta_google_review_url', 1);

		$html .= '
			<div class="google-review-provider">
				<a href="' . $googleReviewUrl . '"><img src="' . $providerImageUrl . '" aria-label="Photo of ' . $providerName . '"></a>
				<button type="button" onclick="window.open(\'' . $googleReviewUrl . '\')" class="content-btn" aria-label="Click to review ' . $providerName . '">' . $providerName . '</button>
			</div>';

		return $html;
	}


	/**
	 * shortcode [cvfmp_google_reviews]
	 *
	 * @since    1.8.00
	 */
	public function google_reviews_shortcode() {
		global $post;
		$postParent = get_post($post->post_parent);
		$providers = $this->get_all_providers();
		$sortedProviders = [];
		$unsortedProviders = [];

		$locationPostTitle = ( $postParent != null ) ? $postParent->post_title : '';

		$html = '
			<section id="google-reviews">
				<div id="google-reviews-header" class="google-reviews-logo">
					<h3>Please leave a review!</h3>
					<p>Click on the name of a provider for whom you would like to leave a review.<br>
						You may be asked to login to your Google Account.</p>
				</div>';
		$html .='
			<div id="google-reviews-list">';

		foreach($providers as $provider) {
			$googleReviewLocations = get_post_meta( $provider->ID, '_cvfmp_meta_google_review_locations', true);
			is_array($googleReviewLocations) ? : $googleReviewLocations = [];
			$isGoogleEnabled = get_post_meta( $provider->ID, '_cvfmp_meta_enable_google_review', 1);
			$googleReviewPosition = get_post_meta( $provider->ID, '_cvfmp_meta_google_review_position', 1);

			if ( in_array($locationPostTitle, $googleReviewLocations) || ( $isGoogleEnabled == true ) ) {
				$provider->position = $googleReviewPosition;

				if ( $googleReviewPosition > 0 ) {
					array_push( $sortedProviders, $provider );
				} else {
					array_push( $unsortedProviders, $provider );
				}
			}
		}

		$sortedProviders = wp_list_sort($sortedProviders, 'position', 'ASC', true);

		foreach ($sortedProviders as $provider) {
			$html .= $this->create_google_reviews_item($provider);
		}

		foreach ($unsortedProviders as $provider) {
			$html .= $this->create_google_reviews_item($provider);
		}

		$html .= '
				</div>
				<p class="google-review-thanks">Thank You!</p>
			</section>';
	
		return $html;
	}


	// Services

	/**
	 * Prints the service info live page.
	 *
	 * @param WP_Post $post The object for the current post/page.
	 */
	public function insert_cvfmp_service ( $content )
	{
		global $post;
		$parentName = get_the_title($post->post_parent);
		$hasShorcode = has_shortcode( $post->post_content, 'cvfmp_service_providers' );

    // Add to page content if parent page is "Services" or "What We Do" and does not have a 'cvfmp_service_providers' shortcode in body.
		if ( $parentName == "Services" || $parentName == "What We Do" && $post->post_title != "Services" || $post->post_title != "What We Do" && $hasShorcode == false ) {

			$serviceImageUrl = get_post_meta( $post->ID, '_cvfmp_meta_service_image_url', 1);
			$serviceDetails = get_post_meta( $post->ID, '_cvfmp_meta_service_details', 1);
			$serviceAdditionalResources = get_post_meta( $post->ID, '_cvfmp_meta_service_additional_resources', 1);
			is_array($serviceAdditionalResources) ? : $serviceAdditionalResources = [];
			$serviceProvidersIntro = get_post_meta( $post->ID, '_cvfmp_meta_service_providers_intro', 1);

			!empty($serviceProvidersIntro) ? : $serviceProvidersIntro = 'To learn more about ' . $post->post_title . ', please schedule an appointment.';


			$serviceContent = '
				<article id="service">
			    <section class="serviceDetail">';

			if (!empty( $serviceImageUrl )) {
				$serviceContent .= '
			      <img src="' . esc_attr( $serviceImageUrl ) . '" alt="[service_page_title]" />';

			}

			$serviceContent .= wpautop(stripcslashes( $serviceDetails ));

			if(!empty($serviceAdditionalResources)) {
				$serviceContent .= '
			      <div class="additionalResources">
			        <strong>Additional Resources:</strong>
			          <ul>';

				foreach($serviceAdditionalResources as $resource) {
					$serviceContent .= '
			            <li><a href="[additional_resource_01_path]">[additional_resource_01_text]</a></li>';
				}

			$serviceContent .= '
			          </ul>
			      </div>';
			}

			$serviceContent .= '
			    </section>';


			$serviceContent .= '
						[cvfmp_service_providers title="' . esc_attr( $serviceProvidersIntro ) . '"]';

			$serviceContent .= '
				</article>
			';

			$content = $serviceContent . $content;
		}


		return $content;
	}


}
