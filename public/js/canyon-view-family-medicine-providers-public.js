(function( $ ) {
	'use strict';

	$(document).ready(function() {
    // Configure/customize these variables.
    var ellipsestext = "...";
    var moretext = "Show more ▼";
		var lesstext = "Show less ▲";
		
		$('.read-more').each(function() {
			var showPars = $(this).attr('fold-count');  // How many paragraphs are above the fold?
			var paragraphs = $(this).find('p');
			var parCount = paragraphs.length;
			var table = '';
			
			if ( $(this).find('table').length ) {
				table = $(this).find('table').prop('outerHTML');
			}
			
			// Truncate content longer than permitted character count
			if ( parCount > showPars ) {
				var featureText = '';
				var foldText = '';
				var tableHeader = '';
				var tableContent = '';
				var html = '';

				for ( var i = 0; i < parCount; i++ ) {
						var paragraph = paragraphs[i].innerHTML;

						if ( i < showPars ) {
							featureText += '<p>' + paragraph + '</p>';
						} else {
							foldText += '<p>' + paragraph + '</p>';
						}
				}
				
				html = featureText;
				html += '<div class="read-more-fold">';
				html += foldText;

				if ( table !== '' ) {
					tableHeader = $(this).find('.tablepress-table-name').prop('innerHTML');
					tableContent = '<h5>' + tableHeader + '</h5>';
					tableContent += table;
					html += tableContent;
				}
				
				html += '</div>';

				$(this).context.innerHTML = html;

			} else {
				$(this).parent().find(".read-more-text").toggle();
			}
			return false;
		});
    
    $(".read-more-link").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
				}

				$(this).parent().prev().find(".read-more-fold").toggle();
        return false;
    });
});


})( jQuery );
